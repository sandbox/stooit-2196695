INTRODUCTION
------------
Display Suite Inception is a drag and drop layout engine for Display Suite.

Allows for easy layout management of all Drupal entities rendered using Display
Suite. Watch the screencast linked below for a full understanding of how to
best use Inception.

 * For a full description of the module, visit the project page:
   https://drupal.org/sandbox/firecannon/2196695


 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2196695


REQUIREMENTS
------------
This module requires the following modules:
 * Display Suite (https://drupal.org/project/ds)
 * Views (https://drupal.org/project/views)
 * Chaos Tools (https://drupal.org/project/ctools)
 * Entity API (https://www.drupal.org/project/entity)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:


   - Define Inception layout for vocabulary/term/content type

     Allows a user to select which layout to use as an override for any given
     Vocabulary, Taxonomy Term or Content Type.


   - Edit Inception Pages

     Allows a user to create and modify pages using the Inception engine.
     This permission is required to add/edit/modify Inception elements on any
     Inception-enabled entity using the admin interface.


   - Edit Inception fields directly

     Allows a user to edit Inception metadata stored directly against an
     Inception-enabled entity.


 * Customize the default Edit/Preview mode settings in Administration » 
   Configuration » User interface » Display Suite Inception.


TROUBLESHOOTING
---------------
 * If the Display Suite Inception administrative interface does not display,
   check the following:

   - Does the logged in user have the appropriate 'Edit Inception Pages'
     permission enabled?

   - Is the entity being rendered using Display Suite?  Ensure that a layout
     is selected at the bottom of the 'Manage Display' form for the given
     entity type.


GETTING STARTED
---------------
 * Creating 'Inception' layout pages (i.e a site front page)

  - This module installs a new 'Inception' content type upon install.  This lets
    you immediately create pages rendered using the Display Suite layouts that
    already exist on your Drupal site.

  - You will see blank regions with an 'Add Element' button in each.  Clicking
    the drop-down provides options to populate the region with (Markup, Blocks,
    Views, Entities, Other Inception entities) with options for each.


 * Using DSI to lay out entity content

  - Simply enable any Drupal entity to be rendered using Display Suite to begin.
    (See Display Suite README)

  - You will see the region contents with entity fields/content as expected. You
    can drag and drop fields around the regions and click the 'Save' button that
    appears from the bottom of the screen.

  - You can also change field formatter settings by hovering over the field and 
    clicking the 'configure' icon from the toolbar.  You can live preview field
    formatter options before saving.

 * Advanced DSI uses (Inception-enabling entities, using inheritance)

  - There is much more to Inception.  The best place to learn is by viewing the
    screencast referenced below.


LINKS
-----
  Project page: http://drupal.org/project/ds_inception
  Screencasts & articles: http://blog.firecannon.com
  Submit bug reports, feature suggestions:
    http://drupal.org/project/issues/ds_inception


MAINTAINERS
-----------
  Stuart Rowlands - https://www.drupal.org/u/firecannon
