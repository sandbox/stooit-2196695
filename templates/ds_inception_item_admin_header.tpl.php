<?php
/**
 * @file
 * Default template implementation to display Inception item admin toolbar.
 */
?>
<div class="inception-object-edit-drag inception-object inception-location-<?php print $location_id; ?>" id="<?php print $item_uuid; ?>">
      <div class="inception-object-toolbar">
        <span class="inception-field-type"><?php print $field_type_title; ?></span>
        <?php if (!$inherited) : ?>
          <a class="ds-inception-link-<?php print $field_type; ?>" href="<?php print $path . $entity_type_link . '/' . $entity_id_link . '/inception/edit/' . $item_uuid . '/' . $field_type; ?>" title="Configure">
            <span class="inception-icon configure"></span>
          </a>
        <?php endif; ?>
        <?php if ($field_type == 'system' && !$inherited) : ?>
          <a href="<?php print $path . $entity_type_link . '/' . $entity_id_link; ?>/edit" title="Edit">
            <span class="inception-icon edit"></span>
          </a>
        <?php endif; ?>
        <?php if ($field_type != 'system' && !$inherited) : ?>
        <a href="<?php print $path . $entity_type_link . '/' . $entity_id_link . '/inception/perm/' . $item_uuid . '/' . $field_type; ?>" title="Permissions">
          <span class="inception-icon permissions"></span>
        </a>
          <a href="<?php print $path . $entity_type_link . '/' . $entity_id_link . '/inception/remove/' . $item_uuid; ?>" title="Remove">
          <span class="inception-icon remove"></span>
        </a>
        <?php endif; ?>
      </div>
  <?php if (!empty($formatter_form)) : ?>
    <?php print $formatter_form; ?>
  <?php endif; ?>
  <div id="inception-field-<?php print $item_uuid; ?>"><?php print $item_markup; ?></div>
</div>
<div class="inception-object-edit-drop inception-location-<?php print $location_id; ?>"><?php print $button_html; ?></div>
