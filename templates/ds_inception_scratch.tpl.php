<?php
/**
 * @file
 * Default template implementation to display Inception scratch.
 */
?>
<div id='inception-dropzone'>
  <span id='inception-scratch-count'>
    <?php print $count; ?>
  </span>
  <span class='inception-icon drop2'></span>SCRATCH
</div>
<div id='inception-scratch'>
  <div id='inception-scratch-drop'></div>
  <?php print $scratch_markup; ?>
</div>
