<?php
/**
 * @file
 * Default template implementation to display field formatter settings.
 */
?>
<div class='field-formatter-settings'>
  <h6>CAUTION:</h6>
  <p>Formatter settings will apply globally for this entity using this view mode.</p>
  <?php print $form; ?>
</div>
