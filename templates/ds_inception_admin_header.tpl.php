<?php
/**
 * @file
 * Default template implementation to display Inception admin header.
 */
?>
<div id='inception-edit-form-holder'>
  <div id='inception-edit-header'>
    <p id='inception-current-layout'>
      <img src='/<?php print $layout_icon; ?>' />
      <span><?php print $layout_label; ?></span>
    </p>
    <div id='inception-edit-link'>
      <?php if ($options['default']): ?>
        <a class='inception-button' href='<?php print $path . $entity_type_link . "/" . $entity_id; ?>'>Edit Inception</a>
      <?php endif; ?>
      <?php if ($options['inception_enabled'] && !$options['default']): ?>
        <a class='inception-button warning' href='<?php print $path . $entity_type_link . "/" . $entity_id; ?>?d=1'>Edit Entity Defaults</a>
        <a class='inception-button warning' href='<?php print $path . $entity_type_link . "/" . $entity_id; ?>/inception/reset-overrides?view_mode=<?php print $view_mode; ?>'>Reset overrides</a>
      <?php endif; ?>
      <a id='inception-preview-toggle' class='inception-button' href='#'>Preview</a>
    </div>
    <?php if ($options['inherited']): ?>
      <div id='inception-inherited-parent'>
        <h4>Inherited layout</h4>
        <p>Modify parent layout <a href='<?php print $path . $primary_layout['alias']; ?>'><?php print $primary_layout['title']; ?></a></p>
      </div>
    <?php endif; ?>
  </div>
</div>
