<?php

/**
 * @file
 * Display Suite Inception main functions.
 */

module_load_include('inc', 'ds_inception', 'includes/ds_inception.helpers');
module_load_include('inc', 'ds_inception', 'includes/ds_inception.form_alter');

/**
 * Implements hook_init().
 */
function ds_inception_init() {
  // Include forms file to ensure field formatter preview works via ajax.
  if ($_GET['q'] == 'system/ajax' && $_POST['form_id'] == 'ds_inception_format_field_form') {
    module_load_include('inc', 'field_ui', 'field_ui.admin');
    module_load_include('inc', 'ds_inception', 'includes/ds_inception.forms');
  }
}

/**
 * Implements hook_help().
 */
function ds_inception_help($path, $arg) {
  module_load_include('inc', 'ds_inception', 'includes/ds_inception.admin');
  return _ds_inception_help($path, $arg);
}

/**
 * Implements hook_theme().
 */
function ds_inception_theme($existing, $type, $theme, $path) {
  module_load_include('inc', 'ds_inception', 'includes/ds_inception.theme');
  return _ds_inception_theme($existing, $type, $theme, $path);
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function ds_inception_ds_layout_settings_info() {
  module_load_include('inc', 'ds_inception', 'includes/ds_inception.ds.defaults');
  return _ds_inception_ds_layout_settings();
}

/**
 * Implements hook_permission().
 */
function ds_inception_permission() {
  module_load_include('inc', 'ds_inception', 'includes/ds_inception.admin');
  return _ds_inception_permission();
}

/**
 * Implements hook_ctools_plugin_api().
 */
function ds_inception_ctools_plugin_api($module, $api) {
  if ($module == 'ds_inception' && $api == 'ds_inception') {
    return array('version' => 1);
  }
  if ($module == 'ds' && $api == 'ds') {
    return array('version' => 1);
  }
}

/**
 * Implements hook_form_alter().
 */
function ds_inception_form_alter(&$form, &$form_state, $form_id) {
  $forms = array(
    'taxonomy_form_term',
    'taxonomy_form_vocabulary',
    'node_type_form',
    'user_admin_settings',
    'inception_node_form',
  );

  if (in_array($form_id, $forms)) {
    module_load_include('inc', 'ds_inception', 'includes/ds_inception.form_alter');
    _ds_inception_form_alter($form, $form_state, $form_id);
  }

  // Move inception fields away in the additional settings group.
  $content_types = node_type_get_types();
  foreach (array_keys($content_types) as $machine_name) {
    if ($form_id == $machine_name . '_node_form' && _ds_inception_is_enabled('node', $machine_name)) {
      _ds_inception_hide_fields($form);
    }
  }
}

/**
 * Implements hook_form_alter().
 *
 * Override ds functions for saving layouts + field settings.
 */
function ds_inception_form_field_ui_display_overview_form_alter(&$form, &$form_state) {

  if (isset($form['#submit'][0]) && $form['#submit'][0] == 'ds_field_ui_layouts_save') {
    $form['#submit'][0] = '_ds_inception_field_ui_layouts_save';
  }
  if (isset($form['#submit'][2]) && $form['#submit'][2] == 'ds_field_ui_fields_save') {
    $form['#submit'][2] = '_ds_inception_field_ui_fields_save';
  }

}

/**
 * Implements hook_entity_info_alter().
 */
function ds_inception_entity_info_alter(&$entity_info) {

  // Attach all default display suite layouts to Inception.
  $layouts = module_invoke_all('ds_layout_info');
  foreach ($layouts as $ds_key => $layout) {
    $entity_info['node']['view modes']['inception_' . $ds_key] = array(
      'label' => $layout['label'],
      'custom settings' => TRUE,
    );

    // Enable custom settings taxonomy terms.
    $entity_info['taxonomy_term']['view modes']['inception_' . $ds_key] = array(
      'label' => $layout['label'],
      'custom settings' => TRUE,
    );

    // Enable custom settings on user entities.
    $entity_info['user']['view modes']['inception_' . $ds_key] = array(
      'label' => $layout['label'],
      'custom settings' => TRUE,
    );
  }

}

/**
 * Implements hook_menu_alter().
 */
function ds_inception_menu_alter(&$items) {
  $items['node/%node']['page callback'] = 'ds_inception_node_page_view';
  $items['node/%node']['file'] = 'includes/ds_inception.pages.inc';
  $items['node/%node']['file path'] = drupal_get_path('module', 'ds_inception');
  $items['taxonomy/term/%taxonomy_term']['page callback'] = 'ds_inception_term_page_view';
  $items['taxonomy/term/%taxonomy_term']['file'] = 'includes/ds_inception.pages.inc';
  $items['taxonomy/term/%taxonomy_term']['file path'] = drupal_get_path('module', 'ds_inception');
  $items['user/%user']['page callback'] = 'ds_inception_user_page_view';
  $items['user/%user']['file'] = 'includes/ds_inception.pages.inc';
  $items['user/%user']['file path'] = drupal_get_path('module', 'ds_inception');
}

/**
 * Implements hook_menu().
 */
function ds_inception_menu() {
  module_load_include('inc', 'ds_inception', 'includes/ds_inception.admin');
  return _ds_inception_menu();
}

/**
 * Implements hook_admin_paths_alter().
 */
function ds_inception_admin_paths_alter(&$paths) {
  $paths['node/*/inception/add/*'] = TRUE;
  $paths['node/*/inception/edit/*'] = TRUE;
  $paths['node/*/inception/perm/*'] = TRUE;
  $paths['node/*/inception/remove/*'] = TRUE;
  $paths['node/*/inception/reset-overrides'] = TRUE;
  $paths['user/*/inception/add/*'] = TRUE;
  $paths['user/*/inception/edit/*'] = TRUE;
  $paths['user/*/inception/perm/*'] = TRUE;
  $paths['user/*/inception/remove/*'] = TRUE;
  $paths['user/*/inception/reset-overrides'] = TRUE;
  $paths['taxonomy/term/*/inception/add/*'] = TRUE;
  $paths['taxonomy/term/*/inception/edit/*'] = TRUE;
  $paths['taxonomy/term/*/inception/perm/*'] = TRUE;
  $paths['taxonomy/term/*/inception/remove/*'] = TRUE;
  $paths['taxonomy/term/*/inception/reset-overrides'] = TRUE;
}

/**
 * Implements hook_preprocess_page().
 */
function ds_inception_preprocess_page(&$vars) {

  if (isset($vars['node']) && isset($vars['node']->field_hide_title) && isset($vars['node']->field_hide_breadcrumbs)) {
    // See if title/breadcrumbs should be disabled first.
    $lang = LANGUAGE_NONE;
    if (isset($vars['node']->field_hide_title[$lang]) && $vars['node']->field_hide_title[$lang][0]['value'] == 1) {
      $vars['title'] = '';
    }
    if (isset($vars['node']->field_hide_breadcrumbs[$lang]) && $vars['node']->field_hide_breadcrumbs[$lang][0]['value'] == 1) {
      drupal_set_breadcrumb(array());
    }
  }

}

/**
 * Implements hook_ds_pre_render_alter().
 */
function ds_inception_ds_pre_render_alter(&$layout_render_array, $context) {

  // @todo: Loaded user entity reflects logged in user rather than viewed.
  if ($context['entity_type'] == 'user' && $context['entity']->uid != arg(1)) {
    $context['entity'] = user_load(arg(1));
  }

  module_load_include('inc', 'ds_inception', 'ds_inception.class');
  $inception_entity = new DsInception($context['entity'], $context['entity_type'], $context['view_mode']);
  $inception_entity->render($layout_render_array);
}

/**
 * Implements hook_node_delete().
 */
function ds_inception_node_delete($node) {
  db_delete('ds_inception_overrides')
    ->condition('inception_id', $node->nid)
    ->condition('inception_type', 'node')
    ->execute();
}

/**
 * Implements hook_taxonomy_term_delete().
 */
function ds_inception_taxonomy_term_delete($term) {
  db_delete('ds_inception_overrides')
    ->condition('inception_id', $term->tid)
    ->condition('inception_type', 'taxonomy_term')
    ->execute();
}

/**
 * Implements hook_user_delete().
 */
function ds_inception_user_delete($account) {
  db_delete('ds_inception_overrides')
    ->condition('inception_id', $account->uid)
    ->condition('inception_type', 'user')
    ->execute();
}
