/**
 * @file
 * Javascript functionality for the Display Suite Inception interface.
 */

(function ($, Drupal, window, document) {

    uuidLocationsOrig = [];
    uuidLocationsNew = [];
    uuids = [];

    function refreshUpdateArray() {

        jQuery.each(Drupal.settings.ds_inception.locations, function (k, location) {
            var il = '.inception-location-' + location + '.inception-object';
            var weight = 1;
            jQuery(il).each(function () {
                var uuid = jQuery(this).attr('id');

                // Only add to update pool if weight or location differs.
                if ((String(uuidLocationsOrig[uuid].location) != String(location)) || (uuidLocationsOrig[uuid].weight != weight)) {
                    uuidLocationsNew[uuid] = [];
                    uuidLocationsNew[uuid].uuid = uuid;
                    uuidLocationsNew[uuid].location = location;
                    uuidLocationsNew[uuid].weight = weight;
                }

                weight++;

                // Ignore changes only within scratch.
                if (String(uuidLocationsOrig[uuid].location) == 'scratch' && String(location) == 'scratch') {
                    delete uuidLocationsNew[uuid];
                }

            });
        });

    }

    jQuery(document).ready(function () {

        Drupal.settings.ds_inception.locations.push('scratch');

        jQuery.each(Drupal.settings.ds_inception.locations, function (k, location) {
            var il = '.inception-location-' + location + '.inception-object';
            var weight = 1;
            jQuery(il).each(function () {
                var uuid = jQuery(this).attr('id');
                // Keep track of original weight and location.
                uuidLocationsOrig[uuid] = [];
                uuidLocationsOrig[uuid].uuid = uuid;
                uuidLocationsOrig[uuid].location = location;
                uuidLocationsOrig[uuid].weight = weight;
                weight++;
            });
        });

        if (Drupal.settings.ds_inception.edit_mode == 0) {
            jQuery('body').addClass('inception-preview');
            jQuery('#inception-preview-toggle').text('Edit mode');
        }

        jQuery('body').append('<div id="ds-inception-footer"><a class="inception-button primary" id="inception-save-layout" href="#">Save</a><a class="inception-button" id="inception-cancel-layout" href="#">Cancel</a></div>');
        jQuery('body').prepend('<div id="ds-inception-overlay"><div id="ds-inception-overlay-load"></div></div>');

        jQuery('.ds-inception-link-system').click(function (e) {
            e.stopImmediatePropagation();
            e.preventDefault();
            jQuery(this).parent().parent().find('.field-formatter-settings').toggle();
        });

        jQuery('#inception-dropzone').click(function () {
            jQuery('#inception-scratch').animate({width: 'toggle'}, 200);
            jQuery('#inception-scratch').css('overflow', '');
        });

        jQuery('#inception-preview-toggle').click(function () {
            jQuery('body').toggleClass('inception-preview');
            jQuery('#inception-preview-toggle').text(
                jQuery('body').hasClass('inception-preview') ? "Edit mode" : "Preview"
            );
            jQuery(".inception-object-edit-drag").draggable(
              jQuery('body').hasClass('inception-preview') ? 'disable' : 'enable'
            );
            jQuery(".inception-object-edit-drag").removeClass("ui-state-disabled");
        });

        jQuery('#inception-edit-toggle').click(function () {
            if (jQuery('#inception-edit-form').is(':visible')) {
                jQuery('#inception-edit-form').slideUp('fast');
                jQuery('.node').fadeTo('fast', 1);
            } else {
                jQuery('#inception-edit-form').slideDown('fast');
                jQuery('.node').fadeTo('fast', 0.5);
            }
        });

        // Hide contextual links for now.
        jQuery('.node-inception .contextual-links-wrapper').hide();

        jQuery('.inception-object-edit-drag').hover(
            function () {
                jQuery(this).find('.inception-object-toolbar').not('.inception-preview').show();
            },
            function () {
                jQuery(this).find('.inception-object-toolbar').hide();
            }
        );

        jQuery(".inception-object-edit-drag").draggable({
            start: handleDragStart,
            cursor: 'move',
            revert: "invalid",
            opacity: 0.4,
            activeClass: "active-drag",
        });
        jQuery(".inception-object-edit-drop").droppable({
            drop: handleDropEvent,
            tolerance: "pointer",
            hoverClass: "selected-drop-zone",
        });

        // Disable draggable in Preview mode
        jQuery(".inception-object-edit-drag").draggable(
          jQuery('body').hasClass('inception-preview') ? 'disable' : 'enable'
        );
        jQuery(".inception-object-edit-drag").removeClass("ui-state-disabled");

        jQuery("#inception-dropzone, #inception-scratch").droppable({
            drop: handleDropzoneEvent,
            tolerance: "touch",
        });

        jQuery('.inception-object-edit-drag').each(function () {
            var classList = jQuery(this).next().attr('class').split(/\s+/);
            var uuid = jQuery(this).attr('id');

            jQuery.each(classList, function (index, item) {
                if (item != 'inception-object-edit-drop' && item != 'ui-droppable') {
                    var obj = {};
                    var location_id = item.replace('inception-location-', '');
                    obj['uuid'] = uuid;
                    obj['location'] = location_id;
                    uuids.push(uuid);
                }
            });
        });

        jQuery('#inception-cancel-layout').click(function () {
          location.reload();
        });

        jQuery('#inception-save-layout').click(function () {

            jQuery('#ds-inception-overlay').fadeIn(400, function () {

                var saveSuccess = true;
                refreshUpdateArray();

                jQuery.each(uuids, function (key, value) {

                    if (uuidLocationsNew[value]) {

                        if (Drupal.settings.ds_inception.entityType == 'taxonomy_term') {
                            var urlBit = 'taxonomy/term/' + Drupal.settings.ds_inception.tid;
                        }

                        if (Drupal.settings.ds_inception.entityType == 'node') {
                            var urlBit = 'node/' + Drupal.settings.ds_inception.nid;
                        }

                        if (Drupal.settings.ds_inception.entityType == 'user') {
                            var urlBit = 'user/' + Drupal.settings.ds_inception.uid;
                        }

                        var inceptionType = Drupal.settings.ds_inception.uuidType[uuidLocationsNew[value].uuid];
                        var inceptionViewMode = Drupal.settings.ds_inception.viewMode;
                        var inceptionDefaultView = Drupal.settings.ds_inception.defaultView;

                        jQuery.ajax({
                            async: false,
                            type: 'POST',
                            dataType: 'json',
                            cache: false,
                            url: Drupal.settings.basePath + urlBit + '/inception/location/' + uuidLocationsNew[value].uuid + '/' + uuidLocationsNew[value].location + '/' + uuidLocationsNew[value].weight + '?type=' + inceptionType + '&view_mode=' + inceptionViewMode + '&default_view=' + inceptionDefaultView,
                            data: {
                                type: inceptionType,
                                viewMode: inceptionViewMode,
                            },
                            success: function (html) {
                                if (html.error != 0) {
                                    alert("Error changing layout: " + html.message);
                                    saveSuccess = false;
                                } else if (html.error == 0) {
                                    jQuery('#ds-inception-footer .inception-button').removeClass('disabled');
                                    jQuery('#ds-inception-footer').slideUp();
                                }
                            }, beforeSend: function () {
                            }
                        });
                    }
                });

                if (saveSuccess) {
                    jQuery('#inception_edit_header').prepend('<div class="messages status">Page successfully saved.</div>');
                    jQuery('#ds-inception-footer').slideUp();
                    jQuery('#ds-inception-overlay').hide();
                }

            });

        });

    });

    function handleDropzoneEvent(event, ui) {
        var dropTarget = jQuery('#inception-scratch-drop');
        ui.draggable.draggable("option", "revert", false);
        var oldPosition = ui.draggable.offset();
        ui.draggable.insertBefore(dropTarget);
        var newPosition = ui.draggable.offset();

        jQuery('#inception-scratch').css('z-index', '50');
        jQuery('#inception-scratch').css('opacity', '1');

        var dropTarget = jQuery('.inception-icon.drop1');
        jQuery(dropTarget).removeClass('drop1');
        jQuery(dropTarget).addClass('drop2');

        // Calculate correct position offset.
        var leftOffset = null;
        var topOffset = null;

        ui.draggable.css('left', '');
        ui.draggable.css('top', '');
        ui.draggable.draggable("option", "revert", true);

        var classList = jQuery(this).attr('class').split(/\s+/);
        var uuid = ui.draggable.attr('id');

        if (!jQuery(ui.draggable).hasClass('inception-location-scratch')) {
            var orig_location_class = 'inception-location-' + uuidLocationsOrig[uuid].location;
            jQuery(ui.draggable).removeClass(orig_location_class);
            jQuery(ui.draggable).addClass('inception-location-scratch');
            var scratchCount = parseInt(jQuery('#inception-scratch-count').html());
            scratchCount++;
            jQuery('#inception-scratch-count').html(scratchCount);
        }

        refreshUpdateArray();

        jQuery('#ds-inception-footer').slideDown('fast');

    }

    function handleDragStart(event, ui) {
        jQuery(ui.draggable).css('opacity', '0.5');
        var dropTarget = jQuery('.inception-icon.drop2');
        jQuery(dropTarget).removeClass('drop2');
        jQuery(dropTarget).addClass('drop1');

        jQuery('#inception-scratch').css('z-index', '0');
        jQuery('#inception-scratch').css('opacity', '0.5');
    }

    function handleDropEvent(event, ui) {
        var dropTarget = jQuery(this);
        var uuid = ui.draggable.attr('id');
        // Find parent uuid & add one to weighting.
        var uuidParent = jQuery(dropTarget).prev().attr('id');

        ui.draggable.draggable("option", "revert", false);
        var oldPosition = ui.draggable.offset();
        ui.draggable.insertBefore(dropTarget);
        var newPosition = ui.draggable.offset();

        jQuery('#inception-scratch').css('z-index', '50');
        jQuery('#inception-scratch').css('opacity', '1');

        var dropTarget = jQuery('.inception-icon.drop1');
        jQuery(dropTarget).removeClass('drop1');
        jQuery(dropTarget).addClass('drop2');

        // Calculate correct position offset.
        var leftOffset = null;
        var topOffset = null;

        ui.draggable.css('left', '');
        ui.draggable.css('top', '');
        ui.draggable.draggable("option", "revert", true);

        var classList = jQuery(this).attr('class').split(/\s+/);

        jQuery.each(classList, function (index, item) {
            if (item != 'inception-object-edit-drop' && item != 'ui-droppable') {
                if (uuidLocationsOrig[uuid].location != item) {
                    var location_id = item.replace('inception-location-', '');

                    var orig_location_class = 'inception-location-' + uuidLocationsOrig[uuid].location;

                    jQuery(ui.draggable).removeClass(orig_location_class);
                    jQuery(ui.draggable).addClass(item);

                    if (location_id != 'scratch' && jQuery(ui.draggable).hasClass('inception-location-scratch')) {
                        jQuery(ui.draggable).removeClass('inception-location-scratch');
                        var scratchCount = parseInt(jQuery('#inception-scratch-count').html());
                        scratchCount--;
                        jQuery('#inception-scratch-count').html(scratchCount);
                    }

                    refreshUpdateArray();
                }
            }
        });

        jQuery('#ds-inception-footer').slideDown('fast');

    }

    // Let mobile users do it too!
    var isMobile = /iPad|iPhone|Android/i.test(navigator.userAgent);
    isMobile && (function ($) {

        var proto = $.ui.mouse.prototype,
            _mouseInit = proto._mouseInit;

        $.extend(proto, {
            _mouseInit: function () {
                this.element
                    .bind("touchstart." + this.widgetName, $.proxy(this, "_touchStart"));
                _mouseInit.apply(this, arguments);
            },

            _touchStart: function (event) {
                this.element
                    .bind("touchmove." + this.widgetName, $.proxy(this, "_touchMove"))
                    .bind("touchend." + this.widgetName, $.proxy(this, "_touchEnd"));

                this._modifyEvent(event);

                // Reset mouseHandled flag in ui.mouse.
                $(document).trigger($.Event("mouseup"));
                this._mouseDown(event);

            },

            _touchMove: function (event) {
                this._modifyEvent(event);
                this._mouseMove(event);
            },

            _touchEnd: function (event) {
                this.element
                    .unbind("touchmove." + this.widgetName)
                    .unbind("touchend." + this.widgetName);
                this._mouseUp(event);
            },

            _modifyEvent: function (event) {
                event.which = 1;
                var target = event.originalEvent.targetTouches[0];
                event.pageX = target.clientX;
                event.pageY = target.clientY;
            }

        });

    })(jQuery);

})(jQuery, Drupal, this, this.document);
