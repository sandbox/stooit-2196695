/**
 * @file
 * Javascript functionality for adding new Display Suite Inception content.
 */

jQuery(document).ready(function () {
    jQuery('.inception-preview-grid-select').click(function () {
        jQuery('.inception-preview-grid-select').css('background', 'none');
        jQuery(this).css('background', '#e2e2e2');
        jQuery('#edit-ds-switch').val(this.id);
    });
});
