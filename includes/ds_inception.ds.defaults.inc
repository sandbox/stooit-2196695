<?php

/**
 * @file
 * Creates a custom inception layout setting for each available ds layout.
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function _ds_inception_ds_layout_settings() {

  ctools_include('export');
  $ds_layout_settings = ctools_export_crud_load_all('ds_layout_settings');

  $ds_layouts = array();
  $layouts = module_invoke_all('ds_layout_info');

  // Only set defaults if ds_layout_settings don't exist.
  foreach ($layouts as $ds_key => $layout) {

    $id = 'node|inception|inception_' . $ds_key;
    if (isset($ds_layout_settings[$id])) {
      continue;
    }

    $ds_layout = new stdClass();
    $ds_layout->api_version = 1;
    $ds_layout->id = $id;
    $ds_layout->entity_type = 'node';
    $ds_layout->bundle = 'inception';
    $ds_layout->view_mode = 'inception_' . $ds_key;
    $ds_layout->layout = $ds_key;
    $ds_layout->settings = array();

    $ds_layouts[$ds_layout->id] = $ds_layout;
  }

  $content_types = node_type_get_types();
  foreach ($content_types as $ct) {
    foreach ($layouts as $ds_key => $layout) {

      $id = 'node|' . $ct->type . '|inception_' . $ds_key;
      if (in_array($id, $ds_layout_settings)) {
        continue;
      }

      $ds_layout = new stdClass();
      $ds_layout->api_version = 1;
      $ds_layout->id = $id;
      $ds_layout->entity_type = 'node';
      $ds_layout->bundle = $ct->type;
      $ds_layout->view_mode = 'inception_' . $ds_key;
      $ds_layout->layout = $ds_key;
      $ds_layout->settings = array();

      $ds_layouts[$ds_layout->id] = $ds_layout;
    }
  }

  $vocabs = taxonomy_get_vocabularies();
  foreach ($vocabs as $v) {
    foreach ($layouts as $ds_key => $layout) {

      $id = 'taxonomy_term|' . $v->machine_name . '|' . 'inception_' . $ds_key;
      if (isset($ds_layout_settings[$id])) {
        continue;
      }

      $ds_layout = new stdClass();
      $ds_layout->api_version = 1;
      $ds_layout->id = $id;
      $ds_layout->entity_type = 'taxonomy_term';
      $ds_layout->bundle = $v->machine_name;
      $ds_layout->view_mode = 'inception_' . $ds_key;
      $ds_layout->layout = $ds_key;
      $ds_layout->settings = array();

      $ds_layouts[$ds_layout->id] = $ds_layout;
    }
  }

  foreach ($layouts as $ds_key => $layout) {

    $id = 'user|user|inception_' . $ds_key;
    if (isset($ds_layout_settings[$id])) {
      continue;
    }

    $ds_layout = new stdClass();
    $ds_layout->api_version = 1;
    $ds_layout->id = $id;
    $ds_layout->entity_type = 'user';
    $ds_layout->bundle = 'user';
    $ds_layout->view_mode = 'inception_' . $ds_key;
    $ds_layout->layout = $ds_key;
    $ds_layout->settings = array();

    $ds_layouts[$ds_layout->id] = $ds_layout;
  }

  return $ds_layouts;
}
