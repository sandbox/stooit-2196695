<?php

/**
 * @file
 * Display Suite Inception theme configuration.
 */

/**
 * Returns theme settings for hook_theme().
 */
function _ds_inception_theme($existing, $type, $theme, $path) {
  return array(
    'ds_inception_formatter_settings_wrapper' => array(
      'template' => 'ds_inception_formatter_settings_wrapper',
      'path' => $path . '/templates',
      'variables' => array(
        'form' => NULL,
      ),
    ),
    'ds_inception_scratch' => array(
      'template' => 'ds_inception_scratch',
      'path' => $path . '/templates',
      'variables' => array(
        'count' => 0,
        'scratch_markup' => NULL,
      ),
    ),
    'ds_inception_admin_header' => array(
      'template' => 'ds_inception_admin_header',
      'path' => $path . '/templates',
      'variables' => array(
        'layout_icon' => NULL,
        'layout_label' => NULL,
        'entity_type_link' => NULL,
        'entity_id' => NULL,
        'view_mode' => 'full',
        'primary_layout' => array(),
        'options' => array(),
      ),
    ),
    'ds_inception_item_admin_header' => array(
      'template' => 'ds_inception_item_admin_header',
      'path' => $path . '/templates',
      'variables' => array(
        'path' => '/',
        'location_id' => NULL,
        'item_uuid' => NULL,
        'entity_type_link' => NULL,
        'entity_id_link' => NULL,
        'field_type' => NULL,
        'field_type_title' => NULL,
        'formatter_form' => NULL,
        'inherited' => FALSE,
      ),
    ),
  );
}
