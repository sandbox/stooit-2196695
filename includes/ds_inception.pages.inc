<?php

/**
 * @file
 * Provides page view override functions to ensure view_mode switching.
 */

/**
 * Menu callback: render node with the DS Inception switching logic.
 */
function ds_inception_node_page_view($node) {

  // If there is a menu link to this node, the link becomes the last part
  // of the active trail, and the link name becomes the page title.
  // Thus, we must explicitly set the page title to be the node title.
  drupal_set_title(check_plain($node->title));
  $uri = entity_uri('node', $node);
  // Set the node path as the canonical URL to prevent duplicate content.
  drupal_add_html_head_link(array(
    'rel' => 'canonical',
    'href' => url($uri['path'], $uri['options']),
  ), TRUE);
  // Set the non-aliased path as a default shortlink.
  drupal_add_html_head_link(array(
    'rel' => 'shortlink',
    'href' => url($uri['path'], array_merge($uri['options'], array('alias' => TRUE))),
  ), TRUE);

  // Update the history table, stating that this user viewed this node.
  node_tag_new($node);

  // For markup consistency with other pages, use node_view_multiple()
  // rather than node_view().
  $view_mode = (!empty($node->ds_switch)) ? $node->ds_switch : 'default';

  // Load inherited Inception node to set view mode if required.
  $layout_override = _ds_inception_get_overrides('content_type', $node->type);

  if (!empty($layout_override)) {
    switch ($layout_override['inception_type']) {
      case 'node':
        $inception_override_entity = node_load($layout_override['inception_id']);
        break;

      case 'taxonomy_term':
        $inception_override_entity = taxonomy_term_load($layout_override['inception_id']);
        break;

      case 'user':
        $inception_override_entity = user_load($layout_override['inception_id']);
        break;
    }
    $view_mode = isset($inception_override_entity->ds_switch) ? $inception_override_entity->ds_switch : $view_mode;
  }

  // If looking at the 'defaults' mode be sure to switch view modes.
  if (isset($_GET['d']) && $_GET['d'] == 1) {
    $view_mode = 'default';
  }

  // It's also possible to use $_GET['v'] to switch view modes.
  if (isset($_GET['v']) && !empty($_GET['v'])) {
    $view_mode = $_GET['v'];
  }
  drupal_static('ds_extras_view_mode', $view_mode);
  return node_view_multiple(array($node->nid => $node), $view_mode);
}

/**
 * Menu callback: render taxonomy term with the DS Inception switching logic.
 */
function ds_inception_term_page_view($term) {

  drupal_set_title($term->name);
  $uri = entity_uri('taxonomy_term', $term);
  // Set the node path as the canonical URL to prevent duplicate content.
  drupal_add_html_head_link(array(
    'rel' => 'canonical',
    'href' => url($uri['path'], $uri['options']),
  ), TRUE);
  // Set the non-aliased path as a default shortlink.
  drupal_add_html_head_link(array(
    'rel' => 'shortlink',
    'href' => url($uri['path'], array_merge($uri['options'], array('alias' => TRUE))),
  ), TRUE);

  $view_mode = (!empty($term->ds_switch)) ? $term->ds_switch : 'default';

  // Load inherited Inception node to set view mode if required.
  $vocab = taxonomy_vocabulary_load($term->vid);
  $layout_override = _ds_inception_get_overrides('taxonomy_term', $vocab->machine_name, $term->tid);
  if (empty($layout_override)) {
    $layout_override = _ds_inception_get_overrides('taxonomy_vocabulary', $vocab->machine_name, $term->vid);
  }

  if (!empty($layout_override)) {
    switch ($layout_override['inception_type']) {
      case 'node':
        $inception_override_entity = node_load($layout_override['inception_id']);
        break;

      case 'taxonomy_term':
        $inception_override_entity = taxonomy_term_load($layout_override['inception_id']);
        break;

      case 'user':
        $inception_override_entity = user_load($layout_override['inception_id']);
        break;
    }
    $view_mode = isset($inception_override_entity->ds_switch) ? $inception_override_entity->ds_switch : $view_mode;
  }

  // If looking at the 'defaults' mode be sure to switch view modes.
  if (isset($_GET['d']) && $_GET['d'] == 1) {
    $view_mode = 'default';
  }

  // It's also possible to use $_GET['v'] to switch view modes.
  if (isset($_GET['v']) && !empty($_GET['v'])) {
    $view_mode = $_GET['v'];
  }

  drupal_static('ds_extras_view_mode', $view_mode);
  return taxonomy_term_view($term, $view_mode);
}

/**
 * Menu callback: render user with the DS Inception switching logic.
 */
function ds_inception_user_page_view($user) {

  $view_mode = (!empty($user->ds_switch)) ? $user->ds_switch : 'full';

  // Load inherited Inception node to set view mode if required.
  $layout_override = _ds_inception_get_overrides('user', 'user');

  if (!empty($layout_override)) {
    switch ($layout_override['inception_type']) {
      case 'node':
        $inception_override_entity = node_load($layout_override['inception_id']);
        break;

      case 'taxonomy_term':
        $inception_override_entity = taxonomy_term_load($layout_override['inception_id']);
        break;

      case 'user':
        $inception_override_entity = user_load($layout_override['inception_id']);
        break;
    }
    $view_mode = isset($inception_override_entity->ds_switch) ? $inception_override_entity->ds_switch : $view_mode;
  }

  // If looking at the 'defaults' mode be sure to switch view modes.
  if (isset($_GET['d']) && $_GET['d'] == 1) {
    $view_mode = 'default';
  }

  if (isset($_GET['v']) && !empty($_GET['v'])) {
    $view_mode = $_GET['v'];
  }

  drupal_static('ds_extras_view_mode', $view_mode);
  return user_view($user, $view_mode);
}
