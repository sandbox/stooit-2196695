<?php

/**
 * @file
 * Display Suite Inception helper functions.
 */

/**
 * Render preview for a Inception Block element.
 */
function _ds_inception_block_preview($form, $form_state) {
  $block_pieces = explode("|", $form['inception_block_select']['#value']);
  $module_type = $block_pieces[0];
  $block_delta = $block_pieces[1];
  $show_title = $form['show_title']['#checked'] ? 1 : 0;
  $classes = $form['container_css_classes']['#value'];
  $element_classes = $form['element_css_classes']['#value'];

  $meta = json_encode(array(
    'moduleType' => $module_type,
    'delta' => $block_delta,
    'classes' => $classes,
    'elementClasses' => $element_classes,
  ));

  return "<div id='inception-block-preview'>" . _ds_inception_render_block(array(
    'meta' => $meta,
    'show_title' => $show_title,
  )) . "</div>";
}

/**
 * Generates a live preview for fields rendered via Display Suite Inception.
 *
 * @param array $form
 *   The form containing values (formatter type + settings) for live preview.
 * @param array $form_state
 *   The current form state.
 *
 * @return string
 *   String return is a rendered preview of the output for a given field.
 *   Fields can be Entity Fields, Display Suite fields or Field-Group output.
 */
function _ds_inception_field_format_preview(array $form, array $form_state) {

  $entity_id = $form['entity_id']['#value'];
  $entity_type = $form['entity_type']['#value'];
  $bundle_name = $form['bundle']['#value'];
  $view_mode = $form['view_mode']['#value'];
  $field_name = $form['field_name']['#value'];
  $label_type = $form['label']['#value'];
  $formatter = $form['formatter']['#value'];

  if ($entity_type == 'node') {
    $entity = node_load($entity_id);
  }
  else {
    if ($entity_type == 'taxonomy_term') {
      $entity = taxonomy_term_load($entity_id);
    }
    else {
      if ($entity_type == 'user') {
        $entity = user_load($entity_id);
      }
    }
  }

  $entity_fields = field_info_instances($entity_type, $bundle_name);

  // System field specific pieces.
  if (isset($entity_fields[$field_name])) {
    // Set instance settings to default formatter & label if hidden.
    $instance = field_read_instance($entity_type, $field_name, $bundle_name);

    // Override from field values.
    $instance['display'][$view_mode]['label'] = $label_type;
    $instance['display'][$view_mode]['type'] = $formatter;

    // Use default settings + override.
    if (!isset($instance['display'][$view_mode]['settings']) || empty($instance['display'][$view_mode]['settings'])) {
      $f = _field_info_collate_types();
      $instance['display'][$view_mode]['settings'] = $f['formatter types'][$formatter]['settings'];
    }

    // Override field settings with form values.
    foreach ($instance['display'][$view_mode]['settings'] as $k => $v) {
      if (isset($form['formatter_settings']['sys'][$k])) {
        $instance['display'][$view_mode]['settings'][$k] = $form['formatter_settings']['sys'][$k]['#value'];
        // Check a second layer deep.
        if (is_array($v)) {
          foreach ($v as $k2 => $v2) {
            $instance['display'][$view_mode]['settings'][$k][$k2] = $form['formatter_settings']['sys'][$k][$k2]['#value'];
          }
        }
      }
      if (isset($form['formatter_settings']['ds'][$k])) {
        $instance['display'][$view_mode]['settings'][$k] = $form['formatter_settings']['ds'][$k]['#value'];
      }
    }

    $instance['display'][$view_mode]['module'] = $f['formatter types'][$formatter]['module'];
    $field_settings = $instance['display'][$view_mode];

  }

  // Check for ds_field_settings against field.
  ctools_include('export');
  $id = $entity_type . '|' . $bundle_name . '|' . $view_mode;
  $ds_field_settings = ctools_export_crud_load_all('ds_field_settings');

  // Check for custom DS field settings for live preview.
  if (!isset($ds_field_settings[$id])) {
    $record = new stdClass();
    $record->id = $id;
    $record->entity_type = $entity_type;
    $record->bundle = $bundle_name;
    $record->view_mode = $view_mode;
    $record->settings = array();
    $ds_field_settings[$id] = $record;
  }

  if (isset($ds_field_settings[$id])) {
    $record = $ds_field_settings[$id];

    // Override label + formatter.
    $record->settings[$field_name]['label'] = $label_type;
    $record->settings[$field_name]['format'] = $formatter;

    // Update non-field template settings.
    $ds_field_info = ds_ds_fields_info($entity_type);
    $field['properties'] = $ds_field_info[$entity_type][$field_name]['properties'];

    foreach ($field['properties']['default'] as $k => $v) {
      $fv = $form['formatter_settings']['ds'][$k]['#value'];
      if ($fv != $record->settings[$field_name]['formatter_settings'][$k]) {
        $record->settings[$field_name]['formatter_settings'][$k] = $fv;
        if (empty($fv)) {
          unset($record->settings[$field_name]['formatter_settings']['ft'][$k]);
        }
      }
    }

    if (isset($form['formatter_settings']['ds']['ft'])) {
      foreach ($form['formatter_settings']['ds']['ft'] as $k => $v) {
        // If the value is empty and not a change.
        if ($v['#value'] != $record->settings[$field_name]['formatter_settings']['ft'][$k]) {
          $record->settings[$field_name]['formatter_settings']['ft'][$k] = $v['#value'];
          if (empty($v['#value'])) {
            unset($record->settings[$field_name]['formatter_settings']['ft'][$k]);
          }
        }
      }
    }
  }

  $output = _ds_inception_render_field($field_name, $entity, $entity_type, $bundle_name, $view_mode, $field_settings, $record->settings[$field_name]);

  if (empty($output)) {
    $output = t('Empty field %t', array('%t' => $field_name));
  }

  return "<div id='inception-field-" . $field_name . "'>" . $output . "</div>";
}

/**
 * Renders a preview for an Inception Entity element.
 */
function _ds_inception_entity_preview($form, $form_state) {
  $entity_ids = $form['entity_id']['#value'];
  $entity_type = $form['entity_type']['#value'];
  $show_title = $form['show_title']['#checked'] ? 1 : 0;
  $view_mode = $form['entity_view_mode']['#value'];
  $classes = $form['container_css_classes']['#value'];
  $element_classes = $form['element_css_classes']['#value'];

  if (_ds_inception_validate_entity_exists($entity_type, $entity_ids)) {

    $entity_render_array = array(
      'value' => $entity_ids,
      'show_title' => $show_title,
      'entity_view_mode' => $view_mode,
      'classes' => $classes,
      'elementClasses' => $element_classes,
    );

    switch ($entity_type) {
      case 'node':
        $entity_output = _ds_inception_render_node($entity_render_array);
        break;

      case 'user':
        $entity_output = _ds_inception_render_user($entity_render_array);
        break;

      case 'taxonomy_term':
        $entity_output = _ds_inception_render_taxonomy_term($entity_render_array);
        break;

    }
    return "<div id='inception-entity-preview'>" . $entity_output . "</div>";
  }
  else {
    return "<div id='inception-entity-preview'>" . t('Entity with that ID does not exist') . "</div>";
  }
}

/**
 * Ajax callback returns info markup when switching field formatter.
 */
function _ds_inception_field_format_settings($form, $form_state) {
  return "<p>" . t('Field formatter settings unavailable until field formatter saved.  You can still preview (with default settings)') . "</p>";
}

/**
 * Returns field formatter settings form for a given field.
 */
function _ds_inception_ajax_formatter_settings($formatter, $entity_type, $field_name, $bundle, $view_mode) {

  $f = _field_info_collate_types();
  $module = isset($f['formatter types'][$formatter]['module']) ? $f['formatter types'][$formatter]['module'] : '';
  $instance = field_read_instance($entity_type, $field_name, $bundle);
  $field = field_info_field($field_name);

  $form = array();
  $form_state = array();

  $settings_form = array();
  $function = $module . '_field_formatter_settings_form';
  if (function_exists($function)) {
    $settings_form = $function($field, $instance, $view_mode, $form, $form_state);
  }

  return $settings_form;

}

/**
 * Renders a preview of an Inception View element.
 */
function _ds_inception_view_preview($form, $form_state) {
  $options = array();
  $options['max_items'] = $form['inception_view_options']['inception_view_max_items']['#value'];
  $options['contextual_filter_value'] = $form['inception_view_options']['inception_contextual_filter_value']['#value'];
  $options['pager_type'] = $form['inception_view_options']['inception_view_pager_type']['#value'];
  $options['title'] = $form['inception_view_options']['inception_view_title']['#value'];
  $options['header_format'] = $form['inception_view_options']['inception_view_header']['#format'];
  $options['header_content'] = $form['inception_view_options']['inception_view_header']['value']['#value'];
  $options['footer_format'] = $form['inception_view_options']['inception_view_footer']['#format'];
  $options['footer_content'] = $form['inception_view_options']['inception_view_footer']['value']['#value'];
  $options['content_type_filter'] = implode(',', array_keys($form['inception_view_options']['inception_content_type_filter']['#value']));

  $view_pieces = explode("|", $form['inception_view_select']['#value']);
  $view_name = $view_pieces[0];
  $view_display_id = $view_pieces[1];

  return "<div id='inception-view-preview'>" . _ds_inception_render_view(array('value' => $view_name), $view_display_id, $options) . "</div>";
}

/**
 * Performs validation for embedding entities.
 */
function _ds_inception_validate_entity_form($element, &$form_state, $form) {
  $entity_id = $element['#value'];
  $entity_type = $form['entity_type']['#value'];

  if (empty($entity_id) || empty($entity_type)) {
    return form_error($element, t('Provide a value'));
  }
}

/**
 * Validate an entity existing for embedding via Inception.
 *
 * @param string $entity_type
 *   The Entity Type to check for.
 * @param int $entity_id
 *   The Entity ID to check for.
 *
 * @return bool
 *   Indicates element to embed validation succeeded or failed.
 */
function _ds_inception_validate_entity_exists($entity_type, $entity_id) {

  if (empty($entity_id) || empty($entity_type)) {
    return FALSE;
  }

  foreach ($entity_id as $eid) {
    if (!$node = node_load($eid)) {
      return FALSE;
    }

    if ($node->type == "inception") {
      return FALSE;
    }
  }

  return TRUE;
}

/**
 * Returns TRUE/FALSE depending on if an Inception element may be viewed.
 *
 * @param string $meta
 *   Meta value contains JSON payload associated with Inception element.
 *
 * @return bool
 *   Whether or not the current user can view this Inception element.
 */
function _ds_inception_view_perm($meta) {
  global $user;

  if ($user && user_access('edit inception page', $user)) {
    return TRUE;
  }

  $can_view = FALSE;
  if (isset($meta->acl) && $meta->acl && isset($meta->acl_role)) {
    foreach ($meta->acl_role as $role_select) {
      $role_pieces = explode("|", $role_select);
      if (in_array($role_pieces[1], $user->roles) || $role_pieces[1] == 'anonymous user') {
        $can_view = TRUE;
      }
    }

    return $can_view;
  }

  // Has no ACL.
  return TRUE;

}

/**
 * Return TRUE/FALSE on if a user has Inception editing permissions.
 */
function _ds_inception_is_admin() {
  return (user_access('edit inception page')) ? TRUE : FALSE;
}

/**
 * Returns available view modes for given entity type.
 */
function _ds_inception_get_entity_view_modes($entity_type) {
  $entity_info = entity_get_info($entity_type);

  $v = array();
  foreach ($entity_info['view modes'] as $key => $view_mode) {
    $v[$key] = $view_mode['label'];
  }

  return $v;

}


/**
 * Returns valid Inception nodes to use as Primary layout override.
 */
function _ds_inception_get_available_overrides($type, $bundle) {

  $bundles = array('inception');

  if ($type == 'node') {
    $bundles[] = $bundle;
  }

  $override_options = array('0' => '-- none --');

  $inception_query = new EntityFieldQuery();
  $inception_query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $bundles, 'IN')
    ->range(0, 200)
    ->addMetaData('account', user_load(1));

  $inception_query_result = $inception_query->execute();

  if (isset($inception_query_result['node'])) {
    $nodes = node_load_multiple(array_keys($inception_query_result['node']));
    foreach ($nodes as $node) {
      $override_options['Node']['node-' . $node->nid] = $node->title . " - " . $node->type;
    }
  }

  if ($type == 'taxonomy_term' && _ds_inception_is_enabled('taxonomy_term', $bundle)) {

    $vocab = taxonomy_vocabulary_machine_name_load($bundle);

    $inception_term_query = new EntityFieldQuery();
    $inception_term_query->entityCondition('entity_type', 'taxonomy_term')
      ->propertyCondition('vid', $vocab->vid)
      ->range(0, 200)
      ->addMetaData('account', user_load(1));

    $inception_term_query_result = $inception_term_query->execute();

    if (isset($inception_term_query_result['taxonomy_term'])) {
      $terms = taxonomy_term_load_multiple(array_keys($inception_term_query_result['taxonomy_term']));
      foreach ($terms as $term) {
        $override_options['Taxonomy Term']['taxonomy_term-' . $term->tid] = $term->name . " - " . $vocab->name;
      }
    }
  }

  if ($type == 'user' && _ds_inception_is_enabled('user', 'user')) {

    $inception_user_query = new EntityFieldQuery();
    $inception_user_query->entityCondition('entity_type', 'user')
      ->range(0, 200)
      ->addMetaData('account', user_load(1));

    $inception_user_query_result = $inception_user_query->execute();

    if (isset($inception_user_query_result['user'])) {
      $users = user_load_multiple(array_keys($inception_user_query_result['user']));
      foreach ($users as $user) {
        if (empty($user->uid)) {
          continue;
        }
        $override_options['Users']['user-' . $user->uid] = $user->name;
      }
    }
  }

  return $override_options;
}


/**
 * Attach Inception fields to vocabulary.
 */
function _ds_inception_attach_to_entity(&$form, $form_state) {

  $entity_type = $form['ds_inception_override']['embed_inception']['entity_type']['#value'];

  if ($entity_type == 'taxonomy_term') {
    $vid = $form['ds_inception_override']['embed_inception']['vocab_id']['#value'];
    $vocab = taxonomy_vocabulary_load($vid);
    $bundle = $vocab->machine_name;
  }
  else {
    if ($entity_type == 'node') {
      $bundle = $form['ds_inception_override']['embed_inception']['node_type']['#value'];
    }
    else {
      if ($entity_type == 'user') {
        $bundle = 'user';
      }
    }
  }

  module_load_include('install', 'ds_inception', 'ds_inception');

  foreach (_ds_inception_installed_fields() as $field) {
    $prior_field = field_info_field($field['field_name']);
    if (empty($prior_field)) {
      field_create_field($field);
    }
    else {
      continue;
    }
  }

  foreach (_ds_inception_installed_instances() as $instance) {
    $instance['bundle'] = $bundle;
    $instance['entity_type'] = $entity_type;

    $prior_instance = field_info_instance($instance['entity_type'], $instance['field_name'], $instance['bundle']);

    if (empty($prior_instance)) {
      field_create_instance($instance);
    }
    else {
      drupal_set_message(t('Instance already attached @fn', array('@fn' => $instance['field_name'])), 'status');
      continue;
    }
  }

  drupal_set_message(t('Successfully attached Inception fields.'), 'status');

}

/**
 * Hide inception fields on node & taxonomy term forms.
 */
function _ds_inception_hide_fields(&$form) {

  $form['ds_inception_fields'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Inception fields'),
    '#weight' => 35,
    '#group' => 'additional_settings',
  );

  $inception_fields = array(
    'field_inception_html',
    'field_inception_block',
    'field_inception_view',
    'field_inception_entity',
    'field_inception_merged_weights',
    'field_inception_generic',
    'field_hide_title',
    'field_hide_breadcrumbs',
  );

  foreach ($inception_fields as $field) {
    if (!user_access('edit inception fields directly') && isset($form[$field])) {
      $form[$field]['#disabled'] = TRUE;
    }

    if (isset($form[$field])) {
      $form['ds_inception_fields'][$field] = $form[$field];
      hide($form[$field]);
    }
  }

  if (!user_access('edit inception fields directly')) {
    $form['ds_inception_fields']['#access'] = FALSE;
  }

}


/**
 * Determine if an entity is inception enabled.
 */
function _ds_inception_is_enabled($entity_type, $bundle) {

  if ($bundle == 'inception') {
    return TRUE;
  }

  $entity_fields = field_info_instances($entity_type, $bundle);
  $inception_fields = _ds_inception_get_inception_fields();
  $is_inception_enabled = FALSE;

  // Check if inception is enabled for this entity (all fields exist).
  foreach ($inception_fields as $field) {
    $is_inception_enabled = isset($entity_fields[$field]) ? TRUE : FALSE;
    if (!$is_inception_enabled) {
      return FALSE;
    }
  }

  return $is_inception_enabled;
}

/**
 * Return array of fields attached to an inception enabled entity.
 */
function _ds_inception_get_inception_fields() {
  return array(
    'field_inception_html',
    'field_inception_block',
    'field_inception_view',
    'field_inception_entity',
    'field_inception_generic',
  );
}

/**
 * Return node id of any inception node overrides (if set).
 */
function _ds_inception_get_overrides($type, $bundle, $entity_id = NULL) {

  // Should only be 1 item to return.
  $q = db_select('ds_inception_overrides', 'o');
  $q->condition('o.bundle', $bundle, '=');
  $q->condition('o.type', $type, '=');
  $q->fields('o', array('inception_id', 'bundle', 'inception_type', 'type'));
  $q->range(0, 1);

  if (!empty($entity_id)) {
    $q->condition('o.entity_id', $entity_id);
  }

  $result = $q->execute();
  $record = $result->fetchAssoc();

  if (isset($record['inception_id'])) {
    return array('inception_type' => $record['inception_type'], 'inception_id' => $record['inception_id']);
  }

  return NULL;

}

/**
 * Set override id for inheritance.
 */
function _ds_inception_set_overrides($type, $bundle, $nid, $inception_type, $entity_id = NULL) {

  // Should only be 1 item to return.
  $q = db_select('ds_inception_overrides', 'o');
  $q->condition('o.bundle', $bundle, '=');
  $q->condition('o.type', $type, '=');
  $q->fields('o', array('id', 'inception_id', 'bundle', 'type'));
  $q->range(0, 1);

  if (!empty($entity_id)) {
    $q->condition('o.entity_id', $entity_id);
  }

  $result = $q->execute();
  $record = $result->fetchAssoc();

  if (isset($record['id'])) {
    if (empty($nid)) {
      db_delete('ds_inception_overrides')
        ->condition('id', $record['id'])
        ->execute();
    }
    else {
      if ($nid != $record['inception_id']) {
        db_update('ds_inception_overrides')
          ->fields(array(
            'type' => $type,
            'bundle' => $bundle,
            'entity_id' => $entity_id,
            'inception_id' => $nid,
            'inception_type' => $inception_type,
          ))
          ->condition('id', $record['id'], '=')
          ->execute();
      }
    }
  }
  else {
    if (isset($nid) && !empty($nid)) {
      db_insert('ds_inception_overrides')
        ->fields(array(
          'type' => $type,
          'bundle' => $bundle,
          'entity_id' => $entity_id,
          'inception_id' => $nid,
          'inception_type' => $inception_type,
        ))->execute();
    }
  }

}


/**
 * Render any Drupal field with DS wrapping as required.
 */
function _ds_inception_render_field($field_name, $entity, $entity_type, $bundle_name, $view_mode, $formatter_settings = array(), $ds_formatter_settings = array(), $show_empty = FALSE) {

  // Entity fields.
  $entity_fields = field_info_instances($entity_type, $bundle_name);

  // Display Suite fields.
  module_load_include('inc', 'ds', 'ds.ds_fields_info');
  $ds_fields = ds_get_fields($entity_type, FALSE);
  $ds_field_info = ds_ds_fields_info($entity_type);
  $id = $entity_type . '|' . $bundle_name . '|' . $view_mode;
  $ds_field_settings = ctools_export_crud_load_all('ds_field_settings');

  // Support for field groups.
  $entity_field_groups = module_exists('field_group') ? field_group_info_groups($entity_type, $bundle_name, $view_mode) : array();

  // If DS field settings exist grab the record.
  $record = isset($ds_field_settings[$id]) ? $ds_field_settings[$id] : array();

  if (empty($record)) {
    $record = new stdClass();
    $record->id = $id;
    $record->entity_type = $entity_type;
    $record->bundle = $bundle_name;
    $record->view_mode = $view_mode;
    $record->settings[$field_name] = array();
  }

  if (!empty($ds_formatter_settings)) {
    $record->settings[$field_name] = array_merge($record->settings[$field_name], $ds_formatter_settings);
  }

  // Render field markup via system.
  if (isset($entity_fields[$field_name])) {
    $instance = field_read_instance($entity_type, $field_name, $bundle_name);
    if (isset($instance['display'][$view_mode]) && is_array($instance['display'][$view_mode])) {
      $formatter_settings = array_merge($instance['display'][$view_mode], $formatter_settings);
    }
    $field_markup = field_view_field($entity_type, $entity, $field_name, $formatter_settings);
    if (isset($field_markup) && !empty($field_markup)) {
      foreach ($field_markup as $k => $v) {
        if (!is_int($k)) {
          continue;
        }
        $items[$k] = $v;
      }
    }
    else {
      if ($show_empty) {
        $field_markup = t('Empty field: %t', array('%t' => $field_name));
        $items[] = array('#markup' => $field_markup);
      }
      else {
        return;
      }
    }
  }

  // Render field markup via DS.
  if (isset($ds_fields[$field_name])) {

    $default_formatter = isset($ds_field_info[$entity_type][$field_name]['properties']['formatters']) ? key($ds_field_info[$entity_type][$field_name]['properties']['formatters']) : '';

    $func = isset($ds_fields[$field_name]['function']) ? $ds_fields[$field_name]['function'] : '';
    $field = isset($record->settings[$field_name]) ? $record->settings[$field_name] : array();
    $field['formatter'] = isset($record->settings[$field_name]['format']) ? $record->settings[$field_name]['format'] : $default_formatter;
    $field['label'] = isset($record->settings[$field_name]['label']) ? $record->settings[$field_name]['label'] : '';
    $field['entity_type'] = $entity_type;
    $field['entity'] = $entity;
    $field['properties'] = isset($ds_field_info[$entity_type][$field_name]['properties']) ? $ds_field_info[$entity_type][$field_name]['properties'] : array();
    $field['formatter_settings'] = isset($record->settings[$field_name]['formatter_settings']) ? $record->settings[$field_name]['formatter_settings'] : array();
    $field['formatter_settings']['vms'] = isset($record->settings[$field_name]['formatter_settings']['vms']) ? $record->settings[$field_name]['formatter_settings']['vms'] : array();
    $field['view_mode'] = $view_mode;

    $field_markup = !empty($func) ? call_user_func($func, $field) : '';
    // Try fallback (unable to render code fields).
    if (empty($field_markup)) {
      $field_markup = ds_get_field_value($field_name, $ds_fields[$field_name], $entity, $entity_type, $bundle_name, $view_mode);
    }

    // Comments get special treatment.
    if (empty($field_markup) && $field_name == 'comments') {
      $c = comment_node_page_additions($entity);
      $field_markup = render($c);
    }

    // If still empty put in placeholder text.
    if (empty($field_markup)) {
      if ($show_empty) {
        $field_markup = t('Empty field: %t', array('%t' => $field_name));
        $items[] = array('#markup' => $field_markup);
      }
      else {
        return;
      }
    }

    $items[] = array('#markup' => $field_markup);
  }

  if (isset($entity_field_groups[$field_name])) {
    // We need to render the whole entity, then run a pre-renderer
    // over the field groups.
    $prerendered_entity = entity_view($entity_type, array($entity), $view_mode);

    if (function_exists('field_group_build_pre_render')) {
      if ($entity_type == 'node') {
        $entity_id = $entity->nid;
      }
      else {
        if ($entity_type == 'taxonomy_term') {
          $entity_id = $entity->tid;
        }
        else {
          if ($entity_type == 'user') {
            $entity_id = $entity->uid;
          }
        }
      }
      $prerendered_entity_groups = field_group_build_pre_render($prerendered_entity[$entity_type][$entity_id]);
    }
    $field_markup = render($prerendered_entity_groups[$field_name]);

    // If still empty put in placeholder text.
    if (empty($field_markup)) {
      if ($show_empty) {
        $field_markup = t('Empty field: %t', array('%t' => $field_name));
        $items[] = array('#markup' => $field_markup);
      }
      else {
        return;
      }
    }

    $items[] = array('#markup' => $field_markup);
  }

  $theme_func = isset($record->settings[$field_name]['formatter_settings']['ft']['func']) ? $record->settings[$field_name]['formatter_settings']['ft']['func'] : 'theme_field';

  if (isset($record->settings[$field_name]['formatter_settings']['ft'])) {
    $variables = array(
      'ds-config' => $record->settings[$field_name]['formatter_settings']['ft'],
    );
  }

  if (isset($entity_fields[$field_name])) {
    $label = $entity_fields[$field_name]['label'];
    $variables['label_hidden'] = (isset($formatter_settings['label']) && $formatter_settings['label'] == 'hidden') ? 1 : 0;
    $variables['element']['#label_display'] = isset($formatter_settings['label']) ? $formatter_settings['label'] : 'hidden';
  }
  else {
    $label = $ds_fields[$field_name]['title'];
    $variables['label_hidden'] = (isset($record->settings[$field_name]['label']) && $record->settings[$field_name]['label'] == 'hidden') ? 1 : 0;
    $variables['element']['#label_display'] = isset($record->settings[$field_name]['label']) ? $record->settings[$field_name]['label'] : 'hidden';
  }

  $label = isset($variables['ds-config']['lb']) ? $variables['ds-config']['lb'] : $label;

  $variables['element']['#field_type'] = 'ds';
  $variables['element']['#entity_type'] = $entity_type;
  $variables['element']['#object'] = '';
  $variables['element']['#field_name'] = $field_name;
  $variables['element']['#bundle'] = $bundle_name;
  $variables['label'] = $label;
  $variables['items'] = $items;

  // Run field variables through template_process_field to get correct field +
  // label markup (from core field.module)
  $variables['field_name_css'] = strtr($variables['element']['#field_name'], '_', '-');
  $variables['field_type_css'] = strtr($variables['element']['#field_type'], '_', '-');
  $variables['classes_array'] = array(
    'field',
    'field-name-' . $variables['field_name_css'],
    'field-type-' . $variables['field_type_css'],
    'field-label-' . $variables['element']['#label_display'],
  );
  // Add a "clearfix" class to the wrapper since we float the label and the
  // field items in field.css if the label is inline.
  if ($variables['element']['#label_display'] == 'inline') {
    $variables['classes_array'][] = 'clearfix';
  }

  // Run through template process_field to attach field classes + label markup.
  template_process_field($variables, '');

  // Perform field render using theme function here.
  $output = call_user_func($theme_func, $variables);

  if (empty($output)) {
    $output = t('Empty field: %t', array('%t' => $label));
  }

  return $output;

}

/**
 * Placeholder function override, just triggers the ds function for now.
 */
function _ds_inception_field_ui_layouts_save(&$form, $form_state) {
  // No need to change the layouts save function just yet.
  ds_field_ui_layouts_save($form, $form_state);
}

/**
 * Override ds_field_ui_fields_save function to allow settings to persist.
 */
function _ds_inception_field_ui_fields_save(&$form, $form_state) {

  // Setup some variables.
  $entity_type = $form['#entity_type'];
  $bundle = $form['#bundle'];
  $view_mode = $form['#view_mode'];

  $field_settings = array();

  // Save settings for each field.
  $fields = $form['#ds_fields'];
  foreach ($fields as $field) {

    // Field settings.
    $field_values = $form_state['values']['fields'][$field];

    // In case the region is hidden, do not save.
    if (isset($field_values['region']) && $field_values['region'] == 'hidden') {
      continue;
    }

    // Build settings.
    $settings = array();
    $settings['weight'] = $field_values['weight'];
    $settings['label'] = $field_values['label'];
    $settings['format'] = $field_values['format']['type'];

    // Any formatter settings.
    if (isset($form_state['formatter_settings'][$field])) {
      $settings['formatter_settings'] = $form_state['formatter_settings'][$field];
    }

    $field_settings[$field] = $settings;
  }

  // Allow other modules to modify the field settings before they get saved.
  drupal_alter('ds_field_settings', $field_settings, $form, $form_state);

  ctools_include('export');
  $id = $form['#export_id'];
  $ds_field_settings = ctools_export_crud_load_all('ds_field_settings');

  $new_record = empty($ds_field_settings[$id]) ? TRUE : FALSE;

  // Save the record.
  if (!empty($field_settings)) {
    $record = new stdClass();
    $record->id = $form['#export_id'];
    $record->entity_type = $entity_type;
    $record->bundle = $bundle;
    $record->view_mode = $view_mode;
    $record->settings = $field_settings;
    if ($new_record) {
      drupal_write_record('ds_field_settings', $record);
    }
    else {
      drupal_write_record('ds_field_settings', $record, 'id');
    }
  }

  // Clear the ds_fields cache.
  cache_clear_all('ds_fields:', 'cache', TRUE);
  cache_clear_all('ds_field_settings', 'cache');

}
