<?php

/**
 * @file
 * Display Suite Inception file returning json for ajax calls.
 */

/**
 * Return available overrides as json.
 */
function ds_inception_available_overrides_ajax($type, $bundle, $string) {

  $bundles = array('inception');

  if ($type == 'node') {
    $bundles[] = $bundle;
  }

  $override_options = array('0' => '-- none --');

  $inception_query = new EntityFieldQuery();
  $inception_query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $bundles, 'IN')
    ->propertyCondition('title', db_like($string) . '%', 'LIKE')
    ->range(0, 30)
    ->addMetaData('account', user_load(1));

  $inception_query_result = $inception_query->execute();

  if (isset($inception_query_result['node'])) {
    $nodes = node_load_multiple(array_keys($inception_query_result['node']));
    foreach ($nodes as $node) {
      $override_options['node-' . $node->nid] = $node->title . " - " . $node->type;
    }
  }

  if ($type == 'taxonomy_term' && _ds_inception_is_enabled('taxonomy_term', $bundle)) {

    $vocab = taxonomy_vocabulary_machine_name_load($bundle);

    $inception_term_query = new EntityFieldQuery();
    $inception_term_query->entityCondition('entity_type', 'taxonomy_term')
      ->propertyCondition('vid', $vocab->vid)
      ->propertyCondition('name', db_like($string) . '%', 'LIKE')
      ->range(0, 30)
      ->addMetaData('account', user_load(1));

    $inception_term_query_result = $inception_term_query->execute();

    if (isset($inception_term_query_result['taxonomy_term'])) {
      $terms = taxonomy_term_load_multiple(array_keys($inception_term_query_result['taxonomy_term']));
      foreach ($terms as $term) {
        $override_options['taxonomy_term-' . $term->tid] = $term->name . " - " . $vocab->name;
      }
    }
  }

  if ($type == 'user' && _ds_inception_is_enabled('user', 'user')) {

    $inception_user_query = new EntityFieldQuery();
    $inception_user_query->entityCondition('entity_type', 'user')
      ->propertyCondition('name', db_like($string) . '%', 'LIKE')
      ->range(0, 30)
      ->addMetaData('account', user_load(1));

    $inception_user_query_result = $inception_user_query->execute();

    if (isset($inception_user_query_result['user'])) {
      $users = user_load_multiple(array_keys($inception_user_query_result['user']));
      foreach ($users as $user) {
        if (empty($user->uid)) {
          continue;
        }
        $override_options['user-' . $user->uid] = $user->name;
      }
    }
  }

  drupal_json_output($override_options);

}
