<?php

/**
 * @file
 * Display Suite Inception callback functions.
 */

/**
 * Page callback for adding new inception element to an entity.
 */
function ds_inception_add_element($inception_node, $location = NULL, $element_type = NULL, $weight = 0) {
  $form = drupal_get_form('ds_inception_add_element_form', $inception_node, $location, $weight, $element_type);
  return drupal_render($form);
}

/**
 * Page callback for removing custom overrides.
 */
function ds_inception_reset_overrides($inception_node, $uuid = NULL) {
  $form = drupal_get_form('ds_inception_reset_overrides_form', $inception_node, $uuid);
  return drupal_render($form);
}

/**
 * Page callback for editing view permissions for an element.
 */
function ds_inception_permissions_element($inception_node, $uuid = NULL, $element_type = NULL) {

  if (empty($element_type) || empty($uuid)) {
    return;
  }

  foreach ($inception_node->$element_type as $field_content) {
    foreach ($field_content as $inception_element) {
      $meta = json_decode($inception_element['meta']);
      if ($meta->uuid == $uuid) {
        $inception_edit = $inception_element;
        break;
      }
    }
  }

  $form = drupal_get_form('ds_inception_permissions_element_form', $inception_node, $inception_edit, $element_type, $uuid);
  return drupal_render($form);
}

/**
 * Page callback for editing inception element attached to an entity.
 */
function ds_inception_edit_element($inception_node, $uuid = NULL, $element_type = NULL) {
  if (empty($element_type) || empty($uuid)) {
    return;
  }

  foreach ($inception_node->$element_type as $field_content) {
    foreach ($field_content as $inception_element) {
      $meta = json_decode($inception_element['meta']);
      if ($meta->uuid == $uuid) {
        $inception_edit = $inception_element;
        break;
      }
    }
  }

  $form = drupal_get_form('ds_inception_edit_element_form', $inception_node, $inception_edit, $element_type, $uuid);
  return drupal_render($form);
}

/**
 * Page callback for removing inception element attached to an entity.
 */
function ds_inception_remove_element($inception_node, $uuid = NULL) {
  if (empty($uuid)) {
    return;
  }

  $form = drupal_get_form('ds_inception_remove_element_form', $inception_node, $uuid);
  return drupal_render($form);
}

/**
 * Page callback for editing location of Inception element attached to entity.
 */
function ds_inception_edit_location($inception_node, $uuid = NULL, $location = NULL, $weight = 0) {

  $error = 0;
  $message = "OK";
  $weight--;

  if (!is_numeric($weight)) {
    $error = 1;
    $message = "Invalid weight provided";
  }

  if (!$uuid || !$location) {
    $error = 1;
    $message = 'UUID or location not set';
  }

  if ($error) {
    exit(json_encode(array('error' => $error, 'message' => $message)));
  }

  if (arg(0) == 'taxonomy') {
    $entity_type = 'taxonomy_term';
    $bundle_name = $inception_node->vocabulary_machine_name;
  }
  else {
    if (arg(0) == 'node') {
      $entity_type = 'node';
      $bundle_name = $inception_node->type;
    }
    else {
      if (arg(0) == 'user') {
        $entity_type = $bundle_name = 'user';
      }
    }
  }

  $is_inception_enabled = _ds_inception_is_enabled($entity_type, $bundle_name);
  $view_mode = (isset($_GET['view_mode']) && !empty($_GET['view_mode'])) ? $_GET['view_mode'] : 'default';
  $default_view = (isset($_GET['default_view'])) ? filter_var($_GET['default_view'], FILTER_VALIDATE_BOOLEAN) : FALSE;

  // Do not run pathauto on inception content changes.
  if (module_exists('pathauto')) {
    $inception_node->path['pathauto'] = FALSE;
  }

  if ($inception_node->type != 'inception') {
    if ($_GET['type'] == 'system' && (!$is_inception_enabled || $default_view)) {

      $id = $entity_type . '|' . $bundle_name . '|' . $view_mode;

      ctools_include('export');
      $ds_field_settings = ctools_export_crud_load_all('ds_field_settings');
      $ds_layout_settings = ctools_export_crud_load_all('ds_layout_settings');
      $field_name = $uuid;

      if (isset($ds_layout_settings[$id])) {
        $layout = $ds_layout_settings[$id];
      }
      else {
        $error = 1;
        $message = "No valid layout for " . $id;
        exit(json_encode(array('error' => $error, 'message' => $message)));
      }

      // Creates/updates record for DS field settings.
      if (!isset($ds_field_settings[$id])) {
        $new_record = TRUE;
        $ds_field_settings[$id] = new stdClass();
        $ds_field_settings[$id]->id = $id;
        $ds_field_settings[$id]->entity_type = $entity_type;
        $ds_field_settings[$id]->bundle = $bundle_name;
        $ds_field_settings[$id]->view_mode = $view_mode;
        $ds_field_settings[$id]->settings = array();
      }
      else {
        $new_record = FALSE;
      }

      $field_settings = $ds_field_settings[$id];

      if (!isset($field_settings->settings[$field_name])) {
        $field_settings->settings[$field_name] = array(
          'weight' => $weight,
          'label' => 'above',
          'format' => 'default',
          'formatter_settings' => array(),
        );
      }
      else {
        $field_settings->settings[$field_name]['weight'] = $weight;
      }

      // Add/update field_settings record.
      if ($new_record) {
        drupal_write_record('ds_field_settings', $field_settings);
      }
      else {
        drupal_write_record('ds_field_settings', $field_settings, 'id');
      }

      // From field_ui (field_ui_display_overview_form_submit)
      $instance = field_read_instance($entity_type, $field_name, $bundle_name);
      $settings = $instance['display'][$view_mode]['settings'];

      // Set instance settings to default formatter & label if hidden.
      if ($instance['display'][$view_mode]['type'] == 'hidden') {
        $instance['display'][$view_mode] = array(
          'label' => 'above',
          'type' => 'default',
          'weight' => $weight,
          'settings' => $settings,
        );
        field_update_instance($instance);
      }
      else {
        if (!empty($instance)) {
          $instance['display'][$view_mode]['weight'] = $weight;
          field_update_instance($instance);
        }
      }

      $record = new stdClass();
      $record->id = $id;
      $record->entity_type = $entity_type;
      $record->bundle = $bundle_name;
      $record->view_mode = $view_mode;
      $record->layout = $layout->layout;
      $record->settings = $layout->settings;

      foreach ($record->settings['regions'] as $rid => $region) {
        foreach ($region as $key => $field) {
          if ($field == $uuid) {
            unset($record->settings['regions'][$rid][$key]);
            unset($record->settings['fields'][$uuid]);
          }
        }
      }

      if (is_array($record->settings['regions'][$location])) {
        array_splice($record->settings['regions'][$location], $weight, 0, array($uuid));
      }
      else {
        $record->settings['regions'][$location] = array($weight => $uuid);
      }

      $record->settings['fields'][$uuid] = $location;

      // Remove old record.
      db_delete('ds_layout_settings')
        ->condition('entity_type', $entity_type)
        ->condition('bundle', $bundle_name)
        ->condition('view_mode', $view_mode)
        ->execute();

      // Save new record.
      if (isset($record) && !empty($record)) {
        drupal_write_record('ds_layout_settings', $record);
      }

    }
  }

  // Ignore if we're altering the raw entity location settings.
  if (!$default_view) {

    $inception_fields = _ds_inception_get_inception_fields();

    foreach ($inception_fields as $inception_field) {
      // Check inception field is attached to the current entity.
      if (!isset($inception_node->$inception_field)) {
        continue;
      }

      $found_uuid = FALSE;

      // Make fields blank arrays if they're null.
      // This should only ever get called on an override where no inception
      // content has been attached to an entity.
      if (empty($inception_node->$inception_field)) {
        if (isset($_GET['type']) && $_GET['type'] == $inception_field) {

          $meta = new stdClass();
          $meta->uuid = $uuid;

          if (!isset($meta->{$view_mode})) {
            $meta->{$view_mode} = new stdClass();
          }

          $meta->{$view_mode}->weight = $weight;
          $meta->{$view_mode}->location_id = $location;
          $meta->{$view_mode}->view_mode = $view_mode;

          $inception_node->{$inception_field}[LANGUAGE_NONE][] = array(
            'value' => 'OVERRIDE',
            'meta' => json_encode($meta),
          );
        }
      }

      // Look for existing UUID.
      foreach ($inception_node->$inception_field as $lang => $field_content) {
        foreach ($field_content as $delta => $metavalue) {
          $meta = json_decode($metavalue['meta']);
          if ($meta->uuid == $uuid) {

            if (!isset($meta->{$view_mode})) {
              $meta->{$view_mode} = new stdClass();
            }

            $meta->{$view_mode}->weight = $weight;
            $meta->{$view_mode}->location_id = $location;
            $meta->{$view_mode}->view_mode = $view_mode;

            $inception_node->{$inception_field}[$lang][$delta]['meta'] = json_encode($meta);
            $found_uuid = TRUE;

          }
        }
      }

      // If unable to match an existing UUID then it must be a first-time
      // location override.  Save the overridden meta location.
      if (isset($_GET['type']) && $_GET['type'] == $inception_field && !$found_uuid) {
        $meta = new stdClass();
        $meta->uuid = $uuid;

        if (!isset($meta->{$view_mode})) {
          $meta->{$view_mode} = new stdClass();
        }

        $meta->{$view_mode}->weight = $weight;
        $meta->{$view_mode}->location_id = $location;
        $meta->{$view_mode}->view_mode = $view_mode;

        $inception_node->{$inception_field}[LANGUAGE_NONE][] = array(
          'value' => 'OVERRIDE',
          'meta' => json_encode($meta),
        );
      }

      // If we can't match an existing UUID and the type is 'system' then
      // set a generic override for the system field.
      if (isset($_GET['type']) && $_GET['type'] == 'system' && !$found_uuid && $inception_field == 'field_inception_generic') {
        $meta = new stdClass();
        $meta->uuid = $uuid;

        if (!isset($meta->{$view_mode})) {
          $meta->{$view_mode} = new stdClass();
        }

        $meta->{$view_mode}->weight = $weight;
        $meta->{$view_mode}->location_id = $location;
        $meta->{$view_mode}->view_mode = $view_mode;

        $inception_node->{$inception_field}[LANGUAGE_NONE][] = array(
          'value' => 'OVERRIDE',
          'html_value' => 'DS Inception override',
          'meta' => json_encode($meta),
        );
      }
    }

    if (!isset($inception_node->field_inception_merged_weights[LANGUAGE_NONE][0]['value'])) {
      $inception_node->field_inception_merged_weights[LANGUAGE_NONE][0]['value'] = '';
    }

    // Update merged weights for individual field.
    if (isset($inception_node->field_inception_merged_weights[LANGUAGE_NONE][0]['value'])) {
      $mw = json_decode($inception_node->field_inception_merged_weights[LANGUAGE_NONE][0]['value']);

      if (!isset($mw->{$view_mode})) {
        $mw->{$view_mode} = new stdClass();
      }

      $mw->{$view_mode}->{$uuid} = $weight;
      $inception_node->field_inception_merged_weights[LANGUAGE_NONE][0]['value'] = json_encode($mw);
    }

    if ($entity_type == 'taxonomy_term') {
      taxonomy_term_save($inception_node);
    }
    else {
      if ($entity_type == 'user') {
        user_save($inception_node);
      }
      else {
        if ($entity_type == 'node') {
          node_save($inception_node);
        }
      }
    }
  }
  else {

    // Clear the ds_fields cache.
    cache_clear_all('ds_fields:', 'cache', TRUE);
    cache_clear_all('ds_field_settings', 'cache');

    // Clear entity info cache.
    cache_clear_all('entity_info', 'cache', TRUE);
    cache_clear_all('field_info_fields', 'cache_field');

    // Clear field caches.
    field_cache_clear();
    field_info_cache_clear();
  }

  exit(json_encode(array('error' => $error, 'message' => $message)));

}
