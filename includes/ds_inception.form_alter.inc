<?php

/**
 * @file
 * Display Suite Inception functions for altering forms.
 */

/**
 * Performs form alter hook functions on inception enabled entities.
 */
function _ds_inception_form_alter(&$form, &$form_state, $form_id) {

  $forms = array(
    'taxonomy_form_term',
    'taxonomy_form_vocabulary',
    'node_type_form',
    'user_admin_settings',
  );

  if (in_array($form_id, $forms) && !isset($form_state['confirm_delete']) && !isset($form_state['confirm_parents'])) {
    if ($form_id == 'taxonomy_form_term' && !empty($form['#vocabulary']->machine_name) && user_access('define inception layout for terms in ' . $form['#vocabulary']->machine_name)) {
      if (!empty($form['#term']['tid'])) {
        _ds_inception_term_override($form);
      }
      _ds_inception_hide_fields($form);

    }
    elseif ($form_id == 'taxonomy_form_vocabulary' && !empty($form['#vocabulary']->machine_name) && user_access('define inception layout for vocabulary ' . $form['#vocabulary']->machine_name)) {
      _ds_inception_vocab_override($form, 'taxonomy_term', $form['#vocabulary']->machine_name);
    }
    elseif ($form_id == 'node_type_form' && !empty($form['#node_type']->type) && user_access('define inception layout for content type ' . $form['#node_type']->type)) {
      _ds_inception_content_type_override($form, 'node', $form['#node_type']->type);
    }
    elseif ($form_id == 'user_admin_settings' && user_access('define inception layout for users')) {
      _ds_inception_user_override($form);
    }
  }

  if ($form_id == 'inception_node_form' && !isset($form['nid']['#value'])) {
    if (_ds_inception_is_admin()) {
      drupal_add_js(drupal_get_path("module", "ds_inception") . "/js/ds_inception_add.js");
      drupal_add_css(drupal_get_path("module", "ds_inception") . "/css/ds_inception.css");
    }

    $form['field_inception_html']['#access'] = FALSE;
    $form['field_inception_view']['#access'] = FALSE;
    $form['field_inception_entity']['#access'] = FALSE;
    $form['field_inception_block']['#access'] = FALSE;

    $ds_vm = ds_entity_view_modes('node');

    $inception_layout_grid = NULL;
    foreach ($ds_vm as $key => $item) {
      $layout = ds_get_layout('node', 'inception', $key, FALSE);
      if (isset($layout['path'])) {
        $fallback_image = drupal_get_path('module', 'ds') . '/images/preview.png';
        $image = (isset($layout['image']) && !empty($layout['image'])) ? $layout['path'] . '/' . $layout['layout'] . '.png' : $fallback_image;
        if (isset($layout['panels']) && !empty($layout['panels']['icon'])) {
          $image = $layout['panels']['path'] . '/' . $layout['panels']['icon'];
        }

        $inception_layout_grid .= "<div class='inception-preview-grid-select' id='" . $layout['view_mode'] . "'><img src='" . file_create_url($image) . "'>";
        $inception_layout_grid .= "<p>" . $layout['label'] . "</p>";
        $inception_layout_grid .= "</div>";
      }
    }

    $form['inception_preview'] = array(
      '#attributes' => array(
        'class' => array('inception_ds_preview_container'),
      ),
      '#markup' => "<div id='inception-preview-grid-holder'>" . $inception_layout_grid . "</div>",
      '#weight' => -10,
    );
  }
}

/**
 * Set an Inception layout override for a Taxonomy Term.
 */
function _ds_inception_term_override(&$form) {

  $vocab = taxonomy_vocabulary_load($form['#term']['vid']);

  $form['ds_inception_override']['override_term'] = array(
    '#type' => 'textfield',
    '#title' => t('Override page layout'),
    '#weight' => -1,
    '#autocomplete_path' => 'ds_inception/overrides_ajax/taxonomy_term/' . $vocab->machine_name,
    '#default_value' => NULL,
  );

  $default_value = _ds_inception_get_overrides('taxonomy_term', $vocab->machine_name, $form['#term']['tid']);

  if (!empty($default_value)) {
    $form['ds_inception_override']['override_term']['#default_value'] = $default_value['inception_type'] . '-' . $default_value['inception_id'];
    $form['ds_inception_override']['override_term']['#description'] = '';
  }
  else {
    // Check for Vocabulary setting if no term override exists.
    $default_value_vocab = _ds_inception_get_overrides('taxonomy_vocabulary', $vocab->machine_name);
    if (!empty($default_value_vocab)) {
      $form['ds_inception_override']['override_term']['#default_value'] = '-- Inherited from Vocab --';
      $form['ds_inception_override']['override_term']['#description'] = t("Inheriting layout from vocabulary settings");
    }
  }

  $form['#submit'][] = '_ds_inception_term_override_submit';

}

/**
 * Alter the Taxonomy form to allow Inception-enable and layout override.
 */
function _ds_inception_vocab_override(&$form, $type, $bundle) {

  $form['ds_inception_override']['override_vocab'] = array(
    '#type' => 'textfield',
    '#title' => t('Override page layout'),
    '#weight' => -1,
    '#autocomplete_path' => 'ds_inception/overrides_ajax/taxonomy_term/' . $form['#vocabulary']->machine_name,
    '#default_value' => NULL,
  );

  $default_value = _ds_inception_get_overrides('taxonomy_vocabulary', $form['#vocabulary']->machine_name, $form['#vocabulary']->vid);

  if (!empty($default_value)) {
    $form['ds_inception_override']['override_vocab']['#default_value'] = $default_value['inception_type'] . '-' . $default_value['inception_id'];
  }

  $form['ds_inception_override']['embed_inception'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embed/Install Inception'),
    '#description' => t('Embed inception fields on this content type for enhanced flexibility.  This functionality allows you to add arbitrary Inception elements (Views / Fields / Blocks / HTML / Existing Entities) to terms within this vocabulary.'),
  );

  $form['ds_inception_override']['embed_inception']['entity_type'] = array(
    '#type' => 'hidden',
    '#value' => 'taxonomy_term',
  );

  $form['ds_inception_override']['embed_inception']['vocab_id'] = array(
    '#type' => 'hidden',
    '#value' => $form['#vocabulary']->vid,
  );

  $form['ds_inception_override']['embed_inception']['install'] = array(
    '#type' => 'submit',
    '#value' => 'Attach Inception fields',
    '#submit' => array('_ds_inception_attach_to_entity'),
  );

  $form['ds_inception_override']['embed_inception']['uninstall'] = array(
    '#type' => 'submit',
    '#value' => 'Remove Inception fields',
    '#submit' => array('_ds_inception_remove_from_entity'),
  );

  $form['#submit'][] = '_ds_inception_vocab_override_submit';
}

/**
 * Set an Inception layout override for a Taxonomy Vocabulary.
 */
function _ds_inception_vocab_override_submit(&$form, $form_state) {

  $vid = $form['#vocabulary']->vid;
  $vocab = taxonomy_vocabulary_load($vid);

  $override = preg_split('/-/', $form['ds_inception_override']['override_vocab']['#value']);
  $inception_type = isset($override[0]) ? $override[0] : NULL;
  $inception_entity_id = isset($override[1]) ? $override[1] : NULL;

  _ds_inception_set_overrides('taxonomy_vocabulary', $vocab->machine_name, $inception_entity_id, $inception_type, $vid);
}

/**
 * Set an Inception layout override Taxonomy Terms.
 */
function _ds_inception_term_override_submit(&$form, $form_state) {

  $tid = $form['#term']['tid'];
  $term = taxonomy_term_load($tid);
  $vocab = taxonomy_vocabulary_load($term->vid);

  $override = preg_split('/-/', $form['ds_inception_override']['override_term']['#value']);
  $inception_type = isset($override[0]) ? $override[0] : NULL;
  $inception_entity_id = isset($override[1]) ? $override[1] : NULL;

  _ds_inception_set_overrides('taxonomy_term', $vocab->machine_name, $inception_entity_id, $inception_type, $tid);
}

/**
 * Attach Inception fields to vocabulary.
 */
function _ds_inception_attach_to_vocab(&$form, $form_state) {
  $vid = $form['ds_inception_override']['embed_inception']['vocab_id']['#value'];
  $vocab = taxonomy_vocabulary_load($vid);

  module_load_include('install', 'ds_inception', 'ds_inception');

  foreach (_ds_inception_installed_instances() as $instance) {
    $instance['bundle'] = $vocab->machine_name;
    $instance['entity_type'] = 'taxonomy_term';

    $prior_instance = field_info_instance($instance['entity_type'], $instance['field_name'], $instance['bundle']);

    if (empty($prior_instance)) {
      field_create_instance($instance);
    }
    else {
      drupal_set_message(t('Instance already attached @fn', array('@fn' => $instance['field_name'])), 'status');
      continue;
    }
  }

}

/**
 * Alter the User Account form to allow Inception-enable and layout override.
 */
function _ds_inception_user_override(&$form) {
  $form['ds_inception_override'] = array(
    '#type' => 'fieldset',
    '#group' => 'additional_settings',
    '#title' => t('Display Suite Inception'),
    '#collapsed' => 1,
    '#collapsible' => 1,
  );

  $form['ds_inception_override']['override_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Override page layout'),
    '#weight' => -1,
    '#autocomplete_path' => 'ds_inception/overrides_ajax/user/user',
    '#default_value' => NULL,
  );

  $default_value = _ds_inception_get_overrides('user', 'user');

  if (!empty($default_value)) {
    $form['ds_inception_override']['override_user']['#default_value'] = $default_value['inception_type'] . '-' . $default_value['inception_id'];
  }

  $form['ds_inception_override']['embed_inception'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embed/Install Inception'),
    '#description' => t('Embed inception fields on this content type for enhanced flexibility.  This functionality allows you to add arbitrary Inception elements (Views / Fields / Blocks / HTML / Existing Entities) to nodes of this content type.'),
  );

  $form['ds_inception_override']['embed_inception']['entity_type'] = array(
    '#type' => 'hidden',
    '#value' => 'user',
  );

  $form['ds_inception_override']['embed_inception']['install'] = array(
    '#type' => 'submit',
    '#value' => 'Attach Inception fields',
    '#submit' => array('_ds_inception_attach_to_entity'),
  );

  $form['ds_inception_override']['embed_inception']['uninstall'] = array(
    '#type' => 'submit',
    '#value' => 'Remove Inception fields',
    '#submit' => array('_ds_inception_remove_from_entity'),
  );

  $form['#submit'][] = '_ds_inception_user_override_submit';
}

/**
 * Set an Inception layout override for user entities.
 */
function _ds_inception_user_override_submit(&$form, $form_state) {

  $override = preg_split('/-/', $form['ds_inception_override']['override_user']['#value']);
  $inception_type = isset($override[0]) ? $override[0] : NULL;
  $inception_entity_id = isset($override[1]) ? $override[1] : NULL;

  _ds_inception_set_overrides('user', 'user', $inception_entity_id, $inception_type);
}

/**
 * Alter the Content Type form to allow Inception-enable and layout override.
 */
function _ds_inception_content_type_override(&$form, $type, $bundle) {

  $form['ds_inception_override'] = array(
    '#type' => 'fieldset',
    '#group' => 'additional_settings',
    '#title' => t('Display Suite Inception'),
    '#collapsed' => 1,
    '#collapsible' => 1,
  );

  $form['ds_inception_override']['override_content_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Override page layout'),
    '#weight' => -1,
    '#autocomplete_path' => 'ds_inception/overrides_ajax/' . $type . '/' . $bundle,
    '#default_value' => NULL,
  );

  $default_value = _ds_inception_get_overrides('content_type', $form['#node_type']->type);
  if (!empty($default_value)) {
    $form['ds_inception_override']['override_content_type']['#default_value'] = $default_value['inception_type'] . '-' . $default_value['inception_id'];
  }

  $form['ds_inception_override']['embed_inception'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embed/Install Inception'),
    '#description' => t('Embed inception fields on this content type for enhanced flexibility.  This functionality allows you to add arbitrary Inception elements (Views / Fields / Blocks / HTML / Existing Entities) to nodes of this content type.'),
  );

  $form['ds_inception_override']['embed_inception']['entity_type'] = array(
    '#type' => 'hidden',
    '#value' => 'node',
  );

  $form['ds_inception_override']['embed_inception']['node_type'] = array(
    '#type' => 'hidden',
    '#value' => $form['#node_type']->type,
  );

  $form['ds_inception_override']['embed_inception']['install'] = array(
    '#type' => 'submit',
    '#value' => 'Attach Inception fields',
    '#submit' => array('_ds_inception_attach_to_entity'),
  );

  $form['ds_inception_override']['embed_inception']['uninstall'] = array(
    '#type' => 'submit',
    '#value' => 'Remove Inception fields',
    '#submit' => array('_ds_inception_remove_from_entity'),
  );

  $form['#submit'][] = '_ds_inception_content_type_override_submit';

}

/**
 * Set Inception layout override for a given content type.
 */
function _ds_inception_content_type_override_submit(&$form, $form_state) {
  $type = $form['#node_type']->type;
  $override = preg_split('/-/', $form['ds_inception_override']['override_content_type']['#value']);
  $inception_type = isset($override[0]) ? $override[0] : NULL;
  $inception_entity_id = isset($override[1]) ? $override[1] : NULL;

  _ds_inception_set_overrides('content_type', $type, $inception_entity_id, $inception_type);
}

/**
 * Implements hook_form_alter().
 */
function ds_inception_form_user_cancel_confirm_form_alter(&$form, &$form_state, $form_id) {

  $uid = $form['_account']['#value']->uid;
  $result = db_query('SELECT id FROM {ds_inception_overrides} o
                    WHERE o.inception_id = :uid
                    AND o.inception_type = :type', array(':uid' => $uid, ':type' => 'user'));

  if ($result->rowCount() != 0) {
    $form['inception_warning'] = array(
      '#markup' => '<strong>WARNING: This entity is being used as an Inception override.</strong>',
    );
  }
}

/**
 * Implements hook_form_alter().
 */
function ds_inception_form_node_delete_confirm_alter(&$form, &$form_state, $form_id) {
  $nid = $form['#node']->nid;
  $result = db_query('SELECT id FROM {ds_inception_overrides} o
                    WHERE o.inception_id = :nid
                    AND o.inception_type = :type', array(':nid' => $nid, ':type' => 'node'));

  if ($result->rowCount() != 0) {
    $form['inception_warning'] = array(
      '#markup' => '<strong>WARNING: This entity is being used as an Inception override.</strong>',
    );
  }
}

/**
 * Implements hook_form_alter().
 */
function ds_inception_form_taxonomy_form_term_alter(&$form, &$form_state, $form_id) {
  if (isset($form_state['confirm_delete'])) {
    $tid = $form['#term']->tid;
    $result = db_query('SELECT id FROM {ds_inception_overrides} o
                    WHERE o.inception_id = :tid
                    AND o.inception_type = :type', array(':tid' => $tid, ':type' => 'taxonomy_term'));

    if ($result->rowCount() != 0) {
      $form['inception_warning'] = array(
        '#markup' => '<strong>WARNING: This entity is being used as an Inception override.</strong>',
      );
    }
  }
}
