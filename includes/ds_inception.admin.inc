<?php

/**
 * @file
 * Registry file for Display Suite Inception.
 */

/**
 * Implements hook_menu().
 */
function _ds_inception_menu() {
  $items = array();

  $items['admin/config/user-interface/inception'] = array(
    'title' => 'Display Suite Inception',
    'description' => 'Configuration items for Display Suite Inception',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ds_inception_config_form'),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['node/%node/inception/reset-overrides'] = array(
    'title' => 'Reset overrides?',
    'page callback' => 'ds_inception_reset_overrides',
    'page arguments' => array(1),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['user/%user/inception/reset-overrides'] = array(
    'title' => 'Reset overrides?',
    'page callback' => 'ds_inception_reset_overrides',
    'page arguments' => array(1),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['taxonomy/term/%taxonomy_term/inception/reset-overrides'] = array(
    'title' => 'Reset overrides?',
    'page callback' => 'ds_inception_reset_overrides',
    'page arguments' => array(2),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['node/%node/inception/reset-weights'] = array(
    'title' => 'Reset weight overrides?',
    'page callback' => 'ds_inception_reset_weights',
    'page arguments' => array(1),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['taxonomy/term/%taxonomy_term/inception/reset-weights'] = array(
    'title' => 'Reset weight overrides?',
    'page callback' => 'ds_inception_reset_weights',
    'page arguments' => array(2),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['taxonomy/term/%taxonomy_term/inception/location'] = array(
    'title' => 'Alter Location (Term Page)',
    'page callback' => 'ds_inception_edit_location',
    'page arguments' => array(2, 5, 6, 7),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['node/%node/inception/location'] = array(
    'title' => 'Alter Location',
    'page callback' => 'ds_inception_edit_location',
    'page arguments' => array(1, 4, 5, 6),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['taxonomy/term/%taxonomy_term/inception/location'] = array(
    'title' => 'Alter Location (Term Page)',
    'page callback' => 'ds_inception_edit_location',
    'page arguments' => array(2, 5, 6, 7),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['user/%user/inception/location'] = array(
    'title' => 'Alter Location',
    'page callback' => 'ds_inception_edit_location',
    'page arguments' => array(1, 4, 5, 6),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['taxonomy/term/%taxonomy_term/inception/add'] = array(
    'title' => 'Add Element',
    'page callback' => 'ds_inception_add_element',
    'page arguments' => array(2, 5, 6, 7),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['node/%node/inception/add'] = array(
    'title' => 'Add Element',
    'page callback' => 'ds_inception_add_element',
    'page arguments' => array(1, 4, 5, 6),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['user/%user/inception/add'] = array(
    'title' => 'Add Element',
    'page callback' => 'ds_inception_add_element',
    'page arguments' => array(1, 4, 5, 6),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['node/%node/inception/remove'] = array(
    'title' => 'Remove Element',
    'page callback' => 'ds_inception_remove_element',
    'page arguments' => array(1, 4),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['user/%user/inception/remove'] = array(
    'title' => 'Remove Element',
    'page callback' => 'ds_inception_remove_element',
    'page arguments' => array(1, 4),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['taxonomy/term/%taxonomy_term/inception/remove'] = array(
    'title' => 'Remove Element',
    'page callback' => 'ds_inception_remove_element',
    'page arguments' => array(2, 5),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['node/%node/inception/perm'] = array(
    'title' => 'Permission to view',
    'page callback' => 'ds_inception_permissions_element',
    'page arguments' => array(1, 4, 5),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['user/%user/inception/perm'] = array(
    'title' => 'Permission to view',
    'page callback' => 'ds_inception_permissions_element',
    'page arguments' => array(1, 4, 5),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['taxonomy/term/%taxonomy_term/inception/perm'] = array(
    'title' => 'Permission to view',
    'page callback' => 'ds_inception_permissions_element',
    'page arguments' => array(2, 5, 6),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['node/%node/inception/edit'] = array(
    'title' => 'Edit Element',
    'page callback' => 'ds_inception_edit_element',
    'page arguments' => array(1, 4, 5),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['user/%user/inception/edit'] = array(
    'title' => 'Edit Element',
    'page callback' => 'ds_inception_edit_element',
    'page arguments' => array(1, 4, 5),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['taxonomy/term/%taxonomy_term/inception/edit'] = array(
    'title' => 'Edit Element',
    'page callback' => 'ds_inception_edit_element',
    'page arguments' => array(2, 5, 6),
    'access arguments' => array('edit inception page'),
    'file' => 'includes/ds_inception.forms.inc',
  );

  $items['ds_inception/overrides_ajax/%/%'] = array(
    'page callback' => 'ds_inception_available_overrides_ajax',
    'file' => 'includes/ds_inception.ajax.inc',
    'page arguments' => array(2, 3, 4),
    'type' => MENU_CALLBACK,
    'access arguments' => array('edit inception page'),
  );

  return $items;

}

/**
 * Implements hook_help().
 */
function _ds_inception_help($path, $arg) {
  switch ($path) {
    case 'admin/help#ds_inception':
      $path = dirname(__FILE__) . '/../README.md';
      if (file_exists($path)) {
        $readme = file_get_contents($path);
      }
      else {
        $path = dirname(__FILE__) . '/../README.txt';
        if (file_exists($path)) {
          $readme = file_get_contents($path);
        }
      }
      if (!isset($readme)) {
        return NULL;
      }
      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];

        if (function_exists($info['process callback'])) {
          $output = $info['process callback']($readme, NULL);
        }
        else {
          $output = '<pre>' . $readme . '</pre>';
        }
      }
      else {
        $output = '<pre>' . $readme . '</pre>';
      }
      return $output;
  }
}

/**
 * Implements hook_permission().
 */
function _ds_inception_permission() {
  $vocabs = taxonomy_vocabulary_get_names();
  $content_types = node_type_get_types();
  $permissions = array();

  foreach ($vocabs as $vocab) {
    $permissions += array(
      'define inception layout for vocabulary ' . $vocab->machine_name => array(
        'title' => t('Define Inception layout for vocabulary @vocab', array('@vocab' => $vocab->name)),
      ),
      'define inception layout for terms in ' . $vocab->machine_name => array(
        'title' => t('Define Inception layout for terms within @vocab', array('@vocab' => $vocab->name)),
      ),
    );
  }

  foreach ($content_types as $ct) {
    $permissions += array(
      'define inception layout for content type ' . $ct->type => array(
        'title' => t('Define Inception layout for content type @ct', array('@ct' => $ct->name)),
      ),
    );
  }

  $permissions += array(
    'edit inception page' => array(
      'title' => t('Edit Inception pages'),
    ),
    'edit inception fields directly' => array(
      'title' => t('Edit Inception fields directly'),
    ),
  );

  return $permissions;
}
